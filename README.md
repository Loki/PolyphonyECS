Polyphony ECS toolkit
=====================

Polyphony is an experimental toolkit demonstrating the use of Entity-Component-System (ECS) to design Graphical User Interfaces (GUI). It also extends the original ECS model to support advanced interfaces.

Installation
------------

```shell
git clone https://gitlab.inria.fr/Loki/PolyphonyECS.git
cd PolyphonyECS
```

### Native dependencies

Polyphony requires [Node.js](https://nodejs.org/) (we recommend LTS, packages break sometimes with latest version).

For macos, the script `setup_mac.sh` takes care of installing Node via [Homebrew](https://brew.sh).

For Linux distributions, we recommend installing those dependencies using the package manager from your distribution. 


### Javascript dependencies

``` shell
npm install
```


Running the demo
----------------

You can find [examples, documentation, and live tutorials here](https://loki.gitlabpages.inria.fr/PolyphonyECS/); or, to try on your local machine:

```shell
npm start
```

And access localhost:3000 in your browser.


ECS extensions
--------------

* Systems are entities → we can model their dependencies and ordering with components
* Devices are also entities → it makes it easy to access their data, and allows the support for multiple devices (mice, keyboards, ...)
* Events are signaled using *temporary* components (deleted at the end of the systems chain) → systems can react to events without callbacks and observer patterns

Painter functionalities
--------------
- Create Shapes by selecting your shape with one of the shape button, and drawing the shape through a drag and drop
- Move and select shapes by selecting the cursor
- Write text in a shape by double clicking one shape and typing your text
- Resize a selected shape through the circular menu
- Re-shape an entity by draging the shape on another shape's button
- Delete a selected shape 
- Save your result or Reset your canvas
- Works with SVG and Canvas, access Canvas rendering using the button

Demos
----------------
Painter: starting page for a painter written in PolyphonyECS
SandBox: SandBox to test writing code with Polyphony enabled

SandBox
----------------
If any changes are made to the editor.js script, it needs to be re-compiled using the following command (to enable the codemirror editor):
```
node_modules/.bin/rollup src/js/editor.js -f es -o bin/editor.bundle.js -p @rollup/plugin-node-resolve --config
```
