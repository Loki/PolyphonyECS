import express from 'express' //including express   
import fs from 'fs';

const app = express(); // Creating instance   
app.use(express.static('./'));

const { SERVER_PORT: port = 3000 } = process.env;

app.listen({ port }, () => {
  console.log(`🚀 Server ready at http://0.0.0.0:${port}`);
});

