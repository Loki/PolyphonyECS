export default {
    input: 'editor.js',
    output: {
        file: 'editor.bundle.js',
        format: 'es',
        name: 'bundle'
    }
};