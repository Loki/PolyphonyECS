﻿<!DOCTYPE html>
<html>
<head>
    <title>Polyphony</title>
    <meta charset="utf-8" />
    <link href=" https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css " rel="stylesheet">
    <link rel="stylesheet" href="../css/blog.css">
    <script>
        let hamburgerCheckBox = function() {
            let isFullSize = document.body.classList.contains("fullSize");
            if(isFullSize){
                document.body.classList.remove("fullSize");
                document.getElementById("menuContainer").classList.remove("fullSize");
            } else {
                document.body.classList.add("fullSize");
                document.getElementById("menuContainer").classList.add("fullSize");
            }
        }

        let codeEditorTabSwitch = function(event, viewName) {
            let trueTarget = event.target.parentElement;
            if(trueTarget.classList.contains("is-active"))
                return;

            let control = trueTarget.parentElement.parentElement.parentElement;
            control.getElementsByClassName("is-active")[0].classList.remove("is-active");
            trueTarget.classList.add("is-active");
            switch(viewName){
                case "codeEditor":
                    control.classList.remove("resultView");
                    control.classList.add("codeEditorView");
                    break;
                case "result":
                    control.classList.remove("codeEditorView");    
                    control.classList.add("resultView");
                    break;
                default:
                    console.error("Impossible viewName used in codeEditorTabSwitch function");
                    break;
            }
        }
    </script>
</head>
<body class="fullSize">
    <aside class="menu" id="NavMenu">
        <svg id="hamburgerIcon" viewbox="0 0 32 32" onclick="hamburgerCheckBox()">
            <rect class="hamburger hamburger-top" width="32" height="2"></rect>
            <rect class="hamburger hamburger-middle" y="15" width="32" height="2"></rect>
            <rect class="hamburger hamburger-bottom" y="30" width="32" height="2"></rect>
        </svg>
        <div id="menuContainer" class="menu-container fullSize">
            <p class="menu-label">
                Index
            </p>
            <ul class="menu-list">
                <li><a class="itemCustom" href="#Overview">Overview</a></li>
                <li><a class="itemCustom" href="#Context">Context</a></li>
                <li><a class="itemCustom" href="#Entities">Entities</a></li>
                <li><a class="itemCustom" href="#Components">Components</a></li>
                <li><a class="itemCustom" href="#Systems">Systems</a></li>
            </ul>
        </div>
    </aside>
    <div id="BlogContent">
        <div id="Title">
            <h1 class="title is-2">How to use Polyphony</h1>
        </div>

        <div id="Overview" class="sectionBlog">
            <h2 class="title is-3">Overview</h2>
            <p>Polyphony is a Javascript framework, based on the ECS architecture, focused on creating user interfaces. <a href="https://en.wikipedia.org/wiki/Entity_component_system">ECS (Entity - Component - System)</a> is a software architecture pattern, separating data from behaviour. On one side, data is represented by entities, holding components that define this entities. On the other side, behavior is defined by systems, which can act on subsets of entities called selections.</p>
            <div id="Schematic">
                <div class="offset-pre"></div>
                <img class="customImage" src="../icons/polyphony_scheme.svg"/>
                <div class="offset-post"></div>
            </div>
            <p class="label">Figure 1: Schematic of a project using PolyphonyECS framework</p>
        </div>

        <div id="Context" class="sectionBlog">
            <h2 class="title is-3">Context</h2>
            <p>Polyphony ECS contains everything related to a canvas inside a <a href="../../docs/module-PolyphonyECS-Context.html">Context</a>. To start, we need to create a context with a name:</p>
            <code>
                let context = new Polyphony.Context("contextName");
            </code>
            <p>Once created, we can reference the context for all our actions.</p>
        </div>
  
        <div id="Entities" class="sectionBlog">
            <h2 class="title is-3">Entities</h2>
            <p>Entities represent an element. Users, devices, buttons and letters are all entities.</p>
            <code>
                let entity = context.createEntity();
            </code>
            <p>But entities by default do not hold any data, this is managed by components and entities are placeholders for those components.</p>
        </div>

        <div id="Components" class="sectionBlog">
            <h2 class="title is-3">Components</h2>
            <p>Components represent <b>the data and properties of an entity.</b> For example, if we consider a circle, it has a shape, a position, a radius, a color, and various other properties.</p>
            <p>Components can be added to an entity as a javascript object attribute. Considering and entity circle, we can add its shape like:</p>
            <code>
                let circle = context.createEntity({<br />
                &nbsp;&nbsp;&nbsp;&nbsp;circle.shape = Polyphony.SHAPE.CIRCLE;<br />
                });
            </code>
            <p>But an entity can <b>evolve over time,</b> at any stage of the program. Components can also for convenience be added or modified as attributes of an entity.</p>
            <code>
                let circle = context.createEntity();<br />
                circle.shape = Polyphony.SHAPE.CIRCLE;<br />
            </code>

            <p>All those components <b>characterize an entity</b> and its properties. Here in this sandbox, we create a square. To be drawn, a shape needs a depth, a bounding box "bounds" and shape. By default the color of the shape is white, so to be visible, we will add a backgroundColor.</p>
            <div class="CodeExample codeEditorView">
                <div class="tabHeader tabs is-boxed">
                    <ul>
                        <li class="is-active" onclick="codeEditorTabSwitch(event, 'codeEditor')"><a>Code</a></li>
                        <li onclick="codeEditorTabSwitch(event, 'result')"><a>Result</a></li>
                    </ul>
                </div>
                <div class="codeEditor" data-code="squareExample"></div>
                <iframe sandbox='allow-scripts allow-same-origin'
                        class="output"
                        srcDoc=''></iframe>
                <button class="runFooter button">Run</button>
            </div>
            <p>We see that components can have a various values, from boolean to complex structures. Polyphony defines structures for some components, like here classes for <a href="../../docs/module-PolyphonyECS.Components.Bounds.html">Bounds</a> and <a href="../../docs/module-PolyphonyECS.Components.Coordinates.html">Coordinates</a> or enumerator for <a href="../../docs/module-PolyphonyECS.html#~SHAPE">SHAPE</a>. Defined components can be found in the <a href="../../docs/index.html">documentation</a>.</p>
            <p>Adding all those components every time can be long as an entity can be described by a lot of components. To ease this, we create factories for a structure.</p>

            <div class="CodeExample">
                <div class="tabHeader tabs is-boxed">
                    <ul>
                        <li class="is-active" onclick="codeEditorTabSwitch(event, 'codeEditor')"><a>Code</a></li>
                        <li onclick="codeEditorTabSwitch(event, 'result')"><a>Result</a></li>
                    </ul>
                </div>
                <div class="codeEditor" data-code="buttonExample">
                </div>
                <iframe sandbox='allow-scripts allow-same-origin'
                        class="output"
                        srcDoc=''></iframe>
                <button class="runFooter button">Run</button>
            </div>

            <p>We can create more complex components, that depend on entities. For example, we want to create a line to link 2 entities, we can create this:</p>

            <div class="CodeExample">
                <div class="tabHeader tabs is-boxed">
                    <ul>
                        <li class="is-active" onclick="codeEditorTabSwitch(event, 'codeEditor')"><a>Code</a></li>
                        <li onclick="codeEditorTabSwitch(event, 'result')"><a>Result</a></li>
                    </ul>
                </div>
                <div class="codeEditor" data-code="lineExample">
                </div>
                <iframe sandbox='allow-scripts allow-same-origin'
                        class="output"
                        srcDoc=''></iframe>
                <button class="runFooter button">Run</button>
            </div>
            <p>Again, factories already defined by the Polyphony framework are described in the documentation under <a href="../../docs/module-PolyphonyECS.Factories.html">factories</a>.</p>
        </div>
 
        <div id="Systems" class="sectionBlog">
            <h2 class="title is-2">Systems</h2>
            <p>Systems will describe the behaviours inside the system. Any recurring events impacting entities.</p>
            <p>Systems are created by registering a function in the <a href="#Context">context</a>, with an order (a positive number) and an <a href="../../docs/module-PolyphonyECS.html#~EVENTS">EVENT</a> type to trigger on. 
            <code>
                context.addSystems(function, order, Polyphony.EVENTS);
            </code>
            To create a MyFunction system running 10th in all systems, to be triggered on pointer events you write:</p>
            <code>
                context.addSystems(MyFunction, 10, Polyphony.EVENTS.POINTER_INPUT);
            </code>
            <p>Systems are run in a <a href="#Context">Context</a> in a sequential order. So system's order can be important to process data from one system to another.
                To illustrate how systems work, we will take the example of a tennis ball launcher. To create it, we need to spawn circles.</p>
   

            <div class="CodeExample">
                <div class="tabHeader tabs is-boxed">
                    <ul>
                        <li class="is-active" onclick="codeEditorTabSwitch(event, 'codeEditor')"><a>Code</a></li>
                        <li onclick="codeEditorTabSwitch(event, 'result')"><a>Result</a></li>
                    </ul>
                </div>
                <div class="codeEditor" data-code="circleSpawner">
                </div>
                <iframe sandbox='allow-scripts allow-same-origin'
                        class="output"
                        srcDoc=''></iframe>
                <button class="runFooter button">Run</button>
            </div>

            <p>Now, we will delete spawned items after some time pass.</p>
            <div class="CodeExample">
                <div class="tabHeader tabs is-boxed">
                    <ul>
                        <li class="is-active" onclick="codeEditorTabSwitch(event, 'codeEditor')"><a>Code</a></li>
                        <li onclick="codeEditorTabSwitch(event, 'result')"><a>Result</a></li>
                    </ul>
                </div>
                <div class="codeEditor" data-code="deleteSpawns">
                </div>
                <iframe sandbox='allow-scripts allow-same-origin'
                        class="output"
                        srcDoc=''></iframe>
                <button class="runFooter button">Run</button>
            </div>

            <p>Now our tennis balls are ready to be launched ! Let's throw them with some intial speed:</p>
            <div class="CodeExample">
                <div class="tabHeader tabs is-boxed">
                    <ul>
                        <li class="is-active" onclick="codeEditorTabSwitch(event, 'codeEditor')"><a>Code</a></li>
                        <li onclick="codeEditorTabSwitch(event, 'result')"><a>Result</a></li>
                    </ul>
                </div>
                <div class="codeEditor" data-code="cannonSpawn">
                </div>
                <iframe sandbox='allow-scripts allow-same-origin'
                        class="output"
                        srcDoc=''></iframe>
                <button class="runFooter button">Run</button>
            </div>

            <p>But we do not launch those tennis balls in space, so we need to add gravity to the system:</p>
            <div class="CodeExample">
                <div class="tabHeader tabs is-boxed">
                    <ul>
                        <li class="is-active" onclick="codeEditorTabSwitch(event, 'codeEditor')"><a>Code</a></li>
                        <li onclick="codeEditorTabSwitch(event, 'result')"><a>Result</a></li>
                    </ul>
                </div>
                <div class="codeEditor" data-code="gravityCannon">
                </div>
                <iframe sandbox='allow-scripts allow-same-origin'
                        class="output"
                        srcDoc=''></iframe>
                <button class="runFooter button">Run</button>
            </div>
        </div>
    </div>
    <script type="module" src="../../bin/codeTutorial.bundle.js"></script>

</body>
</html>



