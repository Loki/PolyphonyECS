import { EditorView, basicSetup } from "codemirror"
import { javascript } from "@codemirror/lang-javascript"
import { keymap } from "@codemirror/view"
import { codeExamples } from "./tutorialCodeBase.js"


let content = `import * as Polyphony from '../js/Polyphony.js';
                
let canvas_candidate = document.getElementById("canvas");
let canvas = Polyphony.Canvas(32, 2, 400, 200);
Polyphony.setRendering(Polyphony.RENDER_CANVAS);
Polyphony.startRendering(canvas_candidate, canvas);

Polyphony.view.bounds.setW(canvas.width).setH(canvas.height);

`;

const baseTheme = EditorView.baseTheme({
    "&light .readOnlyLine": { backgroundColor: "#bbbbbb" },
    "&dark .readOnlyLine": { backgroundColor: "#bbbbbb" }
});

let views = [];
let codeView = document.body.getElementsByClassName("codeEditor");

for (let i = 0; i < codeView.length; i++) {
    let exampleId = codeView[i].getAttribute("data-code");
    let sandbox = codeView[i].parentElement.getElementsByClassName("output")[0];

    views.push(new EditorView({
        doc: codeExamples[exampleId],
        extensions: [basicSetup, javascript()],
        parent: codeView[i]
    }));

    codeView[i].parentElement.getElementsByClassName("runFooter")[0].addEventListener('click', function () { evaluateSrcDoc(views[i], sandbox) });
}

delete keymap['default']['Cmd-L'];
delete keymap['default']['Cmd-T'];
delete keymap['default']['Cmd-W'];
delete keymap['default']['Cmd-J'];
delete keymap['default']['Cmd-R'];

function evaluateSrcDoc(view, sandbox) {
    console.log("ok");
    var code = view.state.doc.toString();

    const webpage = `<!DOCTYPE html>
        <html>
            <head>
                <title>Evalbox's Frame</title>
             
            </head>
            <body>
            </body>
            <script>
                var canvas = document.createElement("canvas");
                canvas.setAttribute("id", "canvas");
                canvas.width = window.innerWidth;
                canvas.height = window.innerHeight
                document.body.appendChild(canvas);
            </script>
            <script type="module">
                import * as Polyphony from '../js/Polyphony.js';

                let context = new Polyphony.Context("default");
                Polyphony.addViewPointerKeyboardToContext(context);
                Polyphony.applyDefaultSelections(context);
                Polyphony.applyDefaultSystems(context);
                            
                let canvas_candidate = document.getElementById("canvas");
                context.canvasEntity = Polyphony.Canvas(context, 0, 0, canvas_candidate.width, canvas_candidate.height, { backgroundColor: [255, 255, 255]});

                context.setCanvas(canvas_candidate);
                context.startRendering();

                Polyphony.startPolyphony();`
        + code + `
            </script>   
        </html>`;

    sandbox.srcdoc = webpage;

    console.log(code);
}