/**
 * Example UML App using PolyphonyECS Architecture
 * Allows creating different classes, linking them
 */

import * as poly from './Polyphony.js';

let context = new poly.Context("UML");
context.canvasEntity = poly.Canvas(context, 32, 2, 800, 800, { backgroundColor: [255, 255, 255]});

let selectButton, classButton, noteButton, linkButton;
let currentLineType;


/**
 * Linked components
 */

let LinkBounds = function(referenceEntity, widthRatio, heightRatio){
    this.referenceEntity = referenceEntity;
    this.widthRatio = widthRatio;
    this.heightRatio = heightRatio;
};
LinkBounds.prototype.setW = function(width){ return this };
LinkBounds.prototype.setH = function(width){ return this };
Object.defineProperty(LinkBounds.prototype, "width", { get() {return Math.abs(this.referenceEntity.bounds.width * this.widthRatio) } });
Object.defineProperty(LinkBounds.prototype, "height", { get() {return Math.abs(this.referenceEntity.bounds.height * this.heightRatio) } });

let LinkCoordinates = function(referenceEntity, xOffset, yOffset, zOffset, xRatio, yRatio, zRatio){
    this.referenceEntity = referenceEntity;
    this.xOffset = xOffset;
    this.yOffset = yOffset;
    this.zOffset = zOffset;
    this.xRatio = xRatio;
    this.yRatio = yRatio;
    this.zRatio = zRatio;
};
LinkCoordinates.prototype.moveTo = function(x, y){
    return;
};
LinkCoordinates.prototype.moveBy = function(x, y){
    return;
};
Object.defineProperty(LinkCoordinates.prototype, "x", { get() {return (this.referenceEntity.coordinates.x + this.referenceEntity.bounds.width * this.xRatio + this.xOffset) } });
Object.defineProperty(LinkCoordinates.prototype, "y", { get() {return (this.referenceEntity.coordinates.y + this.referenceEntity.bounds.height * this.yRatio + this.yOffset) } });
Object.defineProperty(LinkCoordinates.prototype, "z", { get() {return (this.referenceEntity.coordinates.z + this.zOffset) } });


/**
 * Entity factories for each UML Elements
 */

let ClassShape = function(context, width, height, backgroundColor, border, entity = undefined){
    entity = entity || {};
    entity.backgroundColor = null;
    entity.border = border;
    entity = poly.Shape(context, poly.SHAPE.RECTANGLE, width, height, entity);

    let nameField = poly.Shape(context, poly.SHAPE.RECTANGLE, 0, 0, 
        { 
            name: "NAME",
            coordinates: new LinkCoordinates(entity, 0, 0, 0, 0, 0, 0),
            bounds: new LinkBounds(entity, 1, 0.2),
            richText: new poly.RichText("", null, 10, 2, 2, 4, true, {backgroundColor: [100, 100, 100]}),
            backgroundColor: [255, 255, 255],
            border: border,
            UML: true
        });
    let attributeField = poly.Shape(context, poly.SHAPE.RECTANGLE, 0, 0,
        { 
            name: "ATTRIBUTE",
            coordinates: new LinkCoordinates(entity, 0, 0, 0, 0, 0.2, 0),
            bounds: new LinkBounds(entity, 1, 0.4),
            richText: new poly.RichText("", null, 10, 2, 2, 4, true, {backgroundColor: [100, 100, 100]}),
            backgroundColor: [255, 255, 255],
            border: border,
            UML: true
        });
    let methodField = poly.Shape(context, poly.SHAPE.RECTANGLE, 0, 0,
        { 
            name: "METHOD",
            coordinates: new LinkCoordinates(entity, 0, 0, 0, 0, 0.6, 0),
            bounds: new LinkBounds(entity, 1, 0.4),
            richText: new poly.RichText("", null, 10, 2, 2, 4, true, {backgroundColor: [100, 100, 100]}),
            backgroundColor: [255, 255, 255],
            border: border,
            UML: true
        });
    entity.children = [nameField, attributeField, methodField];

    entity.UML = true;
    entity.depth = -1;

    context.addToAllSelections(nameField);
    context.addToAllSelections(attributeField)
    context.addToAllSelections(methodField)
    return entity;
};

let NoteShape = function(context, width, height, backgroundColor, border, entity = undefined){
    entity = entity || {};
    entity.backgroundColor = null;
    entity.border = border;
    entity = poly.Shape(context, poly.SHAPE.RECTANGLE, width, height, entity);

    let noteField = poly.Shape(context, poly.SHAPE.RECTANGLE, 0, 0,
        { 
            name: "ATTRIBUTE",
            coordinates: new LinkCoordinates(entity, 0, 0, 0, 0.5, 0.5, 0),
            bounds: new LinkBounds(entity, 1, 1),
            richText: new poly.RichText("", null, 10, 2, 2, 4, true, {backgroundColor: [100, 100, 100]}),
            backgroundColor: [255, 255, 255],
            border: border,
            UML: true
        });
    entity.children = [noteField];

    entity.UML = true;
    entity.depth = -1;

    return entity;
};

let AssociationLine = function(context, startEntity, endEntity){
    let arrowHead = new poly.CustomShape(context, [[ -20, -10], [0, 0], [-20, 10]], {name: "headGlyph", border: new poly.Border([0, 0, 0], 5)});
    arrowHead.coordinates = endEntity.coordinates;

    let line = poly.LinkLine(context, startEntity, endEntity);
    line.head = arrowHead;
    line.clickable = true;
    line.draggable = true;

    return [line, arrowHead];
};

let InheritanceLine = function(context, startEntity, endEntity){
    let arrowHead = new poly.CustomShape(context, [[ -20, -10], [0, 0], [-20, 10], [-20, -10]], {name: "headGlyph", border: new poly.Border([0, 0, 0], 5)});
    arrowHead.coordinates = endEntity.coordinates;

    let line = poly.LinkLine(context, startEntity, endEntity);
    line.head = arrowHead;
    line.draggable = true;
    line.clickable = true;

    return [line, arrowHead];

};

let ImplementationLine = function(context, startEntity, endEntity){
    let arrowHead = new poly.CustomShape(context, [[ -20, -10], [0, 0], [-20, 10], [-20, -10]], {name: "headGlyph", border: new poly.Border([0, 0, 0], 5)});
    arrowHead.coordinates = endEntity.coordinates;

    let line = new poly.LinkLine(context, startEntity, endEntity);
    line.head = arrowHead;
    line.style = "dotted";
    line.draggable = true;
    line.clickable = true;

    return [line, arrowHead];
};

let DependencyLine = function(context, startEntity, endEntity){
    let arrowHead = new poly.CustomShape(context, [[ -20, -10], [0, 0], [-20, 10]], {name: "headGlyph", border: new poly.Border([0, 0, 0], 5)});
    arrowHead.coordinates = endEntity.coordinates;

    let line = new poly.LinkLine(context, startEntity, endEntity);
    line.head = arrowHead;
    line.style = "dotted";
    line.draggable = true;
    line.clickable = true;

    return [line, arrowHead];
};

let AggregationLine = function(context, startEntity, endEntity){
    let arrowHead = new poly.CustomShape(context, [[ -20, -10], [0, 0], [-20, 10], [-40, 0], [-20, -10]], {name: "headGlyph", border: new poly.Border([0, 0, 0], 5)});
    arrowHead.coordinates = endEntity.coordinates;

    let line = new poly.LinkLine(context, startEntity, endEntity);    
    line.head = arrowHead;
    line.draggable = true;
    line.clickable = true;

    return [line, arrowHead];
};

let CompositionLine = function(context, startEntity, endEntity){
    let arrowHead = new poly.CustomShape(context, [[ -20, -10], [0, 0], [-20, 10], [-40, 0], [-20, -10]], {name: "headGlyph", backgroundColor: [0, 0, 0], border: new poly.Border([0, 0, 0], 5)});
    arrowHead.coordinates = endEntity.coordinates;

    let line = new poly.LinkLine(context, startEntity, endEntity);    
    line.head = arrowHead;
    line.draggable = true;
    line.clickable = true;

    return [line, arrowHead];
};


/**
 * Systems for the UML Application
 */

let DrawUMLSystem = function DrawUMLSystem(context){
    if(!(classButton.toggled || noteButton.toggled)){
        context.getSystem("PointerDragSystem").disabled = false;
        return;
    }
    else {
        context.getSystem("PointerDragSystem").disabled = true;
    }
    
    for(let pointer of context.selections["Pointers"]){
        if(pointer.draw){
            let drawing = pointer.draw;
            drawing.coordinates.moveTo(pointer.pressPosition.x, pointer.pressPosition.y, 0);
            drawing.bounds.setW(Math.abs(pointer.coordinates.x - pointer.pressPosition.x))
                            .setH(Math.abs(pointer.coordinates.y - pointer.pressPosition.y));

            if(pointer.tmpReleased == 0) pointer.draw = null;
        }
        else if(pointer.tmpPressed == 0){
            let drawingType, entity;
            if(classButton.toggled){
                entity = ClassShape(context, 0, 0, [255, 255, 255], new poly.Border([0, 0, 0], 1));
            }
            else if(noteButton.toggled){
                entity = NoteShape(context, 0, 0, [255, 255, 255], new poly.Border([0, 0, 0], 1));
            }
            else{
                continue;
            }
            entity.coordinates = new poly.Coordinates(pointer.coordinates.x, pointer.coordinates.y, 0);
            context.addToAllSelections(entity);
            pointer.draw = entity;
        }
    }
};

let LinkElementSystem = function LinkElementSystem(context){
    if(!linkButton.toggled)
        return;

    for(let pointer of context.selections["Pointers"]){
        if(pointer.link && pointer.tmpPressed == 0 && pointer.target.UML){
            pointer.link.coordinates.endEntity = pointer.target;
            pointer.link.head.coordinates = pointer.target.coordinates;
            pointer.link.depth = -2;
            context.updateAllSelections(pointer.link);
            pointer.link = null;
            pointer.pressTarget = null;
        }
        else if(pointer.tmpPressed == 0 && pointer.target.UML){
            let [link, arrow] = new currentLineType(context, pointer.target, pointer);
            link.backgroundColor = [200, 200, 200];
            link.border =  new poly.Border([100, 100, 100], 2);
            link.depth = 1;
            context.addToAllSelections(link);
            context.addToAllSelections(arrow);
            pointer.link = link;
        }
    }
};

let MultiLayerFocusSystem = function MultiLayerFocusSystem(context) {
	// detect mouse presses on a focusable Entity
	let focus = undefined;
	for (let pointer of context.selections["Pointers"]) {
		let target = pointer.target;
        if(pointer.tmpPressed !== undefined && target != null) 
            focus = findFocusableInChildren(target, pointer.coordinates);
	}
	
	if (focus) {
		// for each keyboard, unfocus the previously focused Entity
		for (let keyboard of context.selections["Keyboards"]) {
			poly.changeFocusedEntity(keyboard, focus);
		}
		
		// make the new Entity visually focused
		let target = focus.richText;
		if (target) {
			target.focused = true;
			let style = target.focusStyle;
			let old_style = target._focusStyle || (target._focusStyle = {});
            poly.ReplaceComponentsInEntity(focus, style, old_style);
		}
	}
};

let LinkTypeShortcutSystem = function LinkTypeShortcutSystem(context) {
    for(let keyboard of context.selections["Keyboards"]){
        if(keyboard.tmpShortcut == "KeyT")
            currentLineType = AggregationLine;
        if(keyboard.tmpShortcut == "KeyY")
            currentLineType = AssociationLine;
        if(keyboard.tmpShortcut == "KeyU")
            currentLineType = CompositionLine;
        if(keyboard.tmpShortcut == "KeyI")
            currentLineType = DependencyLine;
        if(keyboard.tmpShortcut == "KeyO")
            currentLineType = ImplementationLine;
        if(keyboard.tmpShortcut == "KeyP")
            currentLineType = InheritanceLine;    
    }
};

export let findFocusableInChildren = function(entity, pointerCoordinates){
    if(isEntityFocusable(entity))
        return entity;

    if(entity.children) 
        for(let c of entity.children)
            if(poly.Bounds.contains(c, pointerCoordinates))
                return findFocusableInChildren(c, pointerCoordinates);

    return null;
}

export let isEntityFocusable = function(entity){
    return entity && entity.richText && entity.richText.editable;
}

let InputTextSystem = function InputTextSystem(context){
    let keyboard = context.selections["Keyboards"][0];

    for(let pointer of context.selections["Pointers"]){
        if(keyboard.focus && poly.Bounds.contains(keyboard.focus, pointer.coordinates) && pointer.tmpDB){
            keyboard.targetTextfield = keyboard.focus;
        }
    }
};


/**
 * Set Buttons in interface
 */
let createButtonPanel = function(){
    let y = 20;
    let selectStyle = { backgroundColor: [0, 100, 200], border: new poly.Border([50, 50, 50], 1)};

    selectButton = poly.RadioButton(context, new poly.RichText("select", "Arial", 10, 5, 0, false, selectStyle), 60, y+=30, "createUML", selectStyle, {name: "selectButton", depth: -10});
    classButton = poly.RadioButton(context, new poly.RichText("class", "Arial", 10, 5, 0, false, selectStyle), 60, y+=30, "createUML", selectStyle, {name: "classButton", depth: -10});
    noteButton = poly.RadioButton(context, new poly.RichText("note", "Arial", 10, 5, 0, false, selectStyle), 60, y+=30, "createUML", selectStyle, {name: "noteButton", depth: -10});
    linkButton = poly.RadioButton(context, new poly.RichText("link", "Arial", 10, 5, 0, false, selectStyle), 60, y+=30, "createUML", selectStyle, {name: "linkButton", depth: -10});

    setButtonSize(selectButton, 70, 20);
    setButtonSize(classButton, 70, 20);
    setButtonSize(noteButton, 70, 20);
    setButtonSize(linkButton, 70, 20);
};

let setButtonSize = function(entity, width, height){
    entity.richText.width = width;
    entity.richText.height = height;
    entity.bounds.width = width;
    entity.bounds.height = height;
}

window.onload = function() {
    currentLineType = CompositionLine;

    let canvas_candidate = document.getElementById("canvas");
    createButtonPanel();

    poly.addViewPointerKeyboardToContext(context);
    poly.applyDefaultSelections(context);
    poly.applyDefaultSystems(context);
    
    let UMLElements = context.createSelection("UMLElements", e => 'UML' in e);

    context.getSystem("FocusSystem").disabled = true;
    context.addSystem(MultiLayerFocusSystem, poly.EVENTS.POINTER_INPUT, 43);
    context.addSystem(InputTextSystem, poly.EVENTS.POINTER_INPUT, 44);
    context.addSystem(DrawUMLSystem, poly.EVENTS.POINTER_INPUT, 50);
    context.addSystem(LinkElementSystem, poly.EVENTS.POINTER_INPUT, 51);
    context.addSystem(LinkTypeShortcutSystem, poly.EVENTS.KEYBOARD_INPUT, 52);

    context.setCanvas(canvas_candidate);
    context.startRendering();

    poly.startPolyphony();
};