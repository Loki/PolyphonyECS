/**
 * Rendering in Canvas for PolyphonyECS
 * @module renderCanvas
 */

import * as poly from './Polyphony.js'

/**
 * Hold the Canvas Context variable.
 *
 * @type {*}
 */
let canvasContext;

/**
 * Setup canvas for Canvas rendering
 *
 * @param {*} canvas
 * @param {*} canvasWidth
 * @param {*} canvasHeight
 */
let initCanvas = function (canvas, canvasWidth, canvasHeight) {
    canvas.setAttribute("width", canvasWidth);
    canvas.setAttribute("height", canvasHeight);
    canvasContext = canvas.getContext('2d');    
};

/**
 * Render rectangle.
 *
 * @param {*} canvasContext
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} borderColor
 * @param {*} fillColor
 */
function renderRect(canvasContext, x, y, width, height, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    canvasContext.fillStyle = fillColor;
    canvasContext.strokeStyle = borderColor;

    canvasContext.beginPath();
    canvasContext.rect(x, y, width, height);
    canvasContext.fill();
    canvasContext.stroke();    
};

/**
 * Render Ellipse
 *
 * @param {*} canvasContext
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} borderColor
 * @param {*} fillColor
 */
function renderEllipse(canvasContext, x, y, width, height, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    canvasContext.fillStyle = fillColor;
    canvasContext.strokeStyle = borderColor;

    canvasContext.beginPath();
    canvasContext.ellipse(x + width / 2, y + height / 2, width / 2, height / 2, 0, 0, 2 * Math.PI);
    canvasContext.fill();
    canvasContext.stroke();
};

/**
 * Render Line
 *
 * @param {*} canvasContext
 * @param {*} x1
 * @param {*} y1
 * @param {*} x2
 * @param {*} y2
 * @param {*} borderColor
 * @param {*} fillColor
 */
function renderLine(canvasContext, x1, y1, x2, y2, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";
    borderColor = fillColor;

    canvasContext.strokeStyle = borderColor;

    canvasContext.beginPath();
    canvasContext.moveTo(x1, y1);
    canvasContext.lineTo(x2, y2);
    canvasContext.stroke();    
};

/**
 * Render PolyLine.
 *
 * @param {*} canvasContext
 * @param {*} x
 * @param {*} y
 * @param {*} points
 * @param {*} borderColor
 * @param {*} fillColor
 */
function renderPolyLine(canvasContext, x, y, points, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    canvasContext.strokeStyle = borderColor;
    canvasContext.fillStyle = fillColor;

    canvasContext.beginPath();
    canvasContext.moveTo(x + points[0][0], y + points[0][1]);
    for(let i = 0; i < points.length; i++){
        canvasContext.lineTo(x + points[i][0], y + points[i][1]);
    }
    canvasContext.stroke();
    canvasContext.fill();   
}

/**
 * Render Image.
 *
 * @param {*} canvasContext
 * @param {*} src
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} opacity
 */
function renderImg(canvasContext, src, x, y, width, height, opacity) {
    canvasContext.drawImage(src, x, y, width, height);
};

/**
 * Description Text.
 *
 * @param {*} canvasContext
 * @param {*} text
 * @param {*} x
 * @param {*} y
 * @param {*} height
 * @param {*} width
 * @param {*} fillColor
 */
function renderText(canvasContext, text, x, y, height, width, fillColor) {
    if (!fillColor) fillColor = "transparent";

    canvasContext.fillStyle = fillColor

    canvasContext.font = "14px serif";
    canvasContext.fillText(text, x, y + height / 2);
};

/**
 * Set all the listeners to redirect JS Events to Polyphony.
 */
let setEventListeners = function () {
    document.addEventListener("mouseup", function (event) { poly.eventStack.push({ type: "mouseup", event: event }) });
    document.addEventListener("mousedown", function (event) { poly.eventStack.push({ type: "mousedown", event: event }) });
    document.addEventListener("mousemove", function (event) { poly.eventStack.push({ type: "mousemove", event: event }) });
    document.addEventListener("dblclick", function (event) { poly.eventStack.push({ type: "mousedb", event: event }) });
    document.addEventListener("keydown", function (event) { poly.eventStack.push({ type: "keydown", event: event }) });
    document.addEventListener("keyup", function (event) { poly.eventStack.push({ type: "keyup", event: event }) });
    document.addEventListener("wheel", function (event) { poly.eventStack.push({ type: "wheel", event: event }) });
};

/**
 * Generate color string from array
 *
 * @param {*} array
 * @returns {string}
 */
let colorArrayToString = function (array) {
    if (!array)
        return null

    if (array.length == 3)
        return "rgb(" + array[0] + "," + array[1] + "," + array[2] + ")";
    if (array.length == 4)
        return "rgba(" + array[0] + "," + array[1] + "," + array[2] + "," + array[3] + ")";
    return null
};

/**
 * Update existing Canvas from Entity.
 *
 * @param {*} canvasContext
 * @param {Entity} entity
 * @param {*} shape
 * @param {*} image
 * @param {*} borderColor
 * @param {*} backgroundColor
 */
let updateCanvas = function (canvasContext, entity, shape, image, borderColor, backgroundColor) {
    if (shape === poly.SHAPE.RECTANGLE) {
        renderRect(canvasContext, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height, borderColor, backgroundColor);
    }
    else if (shape === poly.SHAPE.ELLIPSE) {
        renderEllipse(canvasContext, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height, borderColor, backgroundColor);
    }
    else if (shape === poly.SHAPE.LINE) {
        renderLine(canvasContext, entity.coordinates.x1, entity.coordinates.y1, entity.coordinates.x2, entity.coordinates.y2, borderColor, backgroundColor);
    }
    else if (shape === poly.SHAPE.POLYLINE){
        renderPolyLine(canvasContext, entity.coordinates.x, entity.coordinates.y, entity.points, borderColor, backgroundColor);
    }

    if (image) {
        renderImg(canvasContext, image.canvasSrc, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height);
    }

    if (entity.richText)
        renderText(canvasContext, entity.richText.str, entity.coordinates.x, entity.coordinates.y, entity.bounds.height, entity.bounds.width, "rgb(0,0,0)");
};

/**
 * Setup rendering environment.
 */
let initRender = function () {
    setEventListeners();
};

/**
 * Software renderer - draws filled shapes, borders, and icons of Entities
 * in painting order.
 * 
 * @param {poly.Context} context 
 */
let SoftRenderSystem = function SoftRenderSystem(context) {
    canvasContext.clearRect(0, 0, context.selections["Views"][0].bounds.width, context.selections["Views"][0].bounds.height);
    for (let entity of context.selections["Drawables"]) {
        // filled shapes
        let backgroundColor = colorArrayToString(entity.backgroundColor);
        let borderColor = (entity.border) ? colorArrayToString(entity.border.color) : null;

        updateCanvas(canvasContext, entity, entity.shape, entity.image, borderColor, backgroundColor);
    }
};
    
export { initRender, initCanvas, colorArrayToString, SoftRenderSystem }