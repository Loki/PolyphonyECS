let squareExample = `let square = context.createEntity({
    shape: Polyphony.SHAPE.RECTANGLE,
    depth: 0,
    coordinates: new Polyphony.Coordinates(50, 50, 0),
    bounds: new Polyphony.Bounds(50, 50, 20, 20),
    backgroundColor: [100, 0, 0]
});
context.addToAllSelections(square);`;

let buttonExample = `let CustomButton = function (context, label, position, bounds, baseEntity = {}) {
    baseEntity.label = label;
    baseEntity.shape = Polyphony.SHAPE.RECTANGLE;
    baseEntity.coordinates = position;
    baseEntity.bounds = bounds;

    baseEntity.depth = baseEntity.depth || 0;
    baseEntity.backgroundColor = baseEntity.backgroundColor || [150, 150, 150];
    baseEntity.borderColor = baseEntity.borderColor || [0, 0, 0];

    return context.createEntity(baseEntity);
}
let customButton = new CustomButton(context, "abc", new Polyphony.Coordinates(20, 20, 0), new Polyphony.Bounds(20, 20, 50, 100));
context.addToAllSelections(customButton)`;

let lineExample = `//Create a line
class LineCoordinates {
    constructor(startEntity, endEntity){
        this.start = startEntity;
        this.end = endEntity;
    }
    // Define as a line
    get x1() { return this.start.coordinates.x };
    get y1() { return this.start.coordinates.y };
    get x2() { return this.end.coordinates.x };
    get y2() { return this.end.coordinates.y };

    // Define as a coordinate
    get x() { return Math.min(x1, x2) };
    get y() { return Math.max(y1, y2) };
}

let startOfLine = Polyphony.Shape(context, Polyphony.SHAPE.RECTANGLE, 40, 40, { backgroundColor: [100, 0, 0] });
let endOfLine = Polyphony.Shape(context, Polyphony.SHAPE.RECTANGLE, 40, 40, { backgroundColor: [100, 0, 0] });
startOfLine.coordinates = new Polyphony.Coordinates(100, 100);
endOfLine.coordinates = new Polyphony.Coordinates(500, 500);

// We link the two rectangles
let line = new Polyphony.Shape(context,
    Polyphony.SHAPE.LINE,
    20,
    20,
    {
      backgroundColor: [100, 0, 0],
      borderColor: [100, 0, 0],
      coordinates: new LineCoordinates(startOfLine, endOfLine)
    }                
);
context.addToAllSelections([startOfLine, endOfLine, line])`;

let circleSpawner = `let timerSpawn = 1;
let previousTime = new Date().getTime() / 1000;

function SpawnSystem(context) {
    let currentTime = new Date().getTime() / 1000;
    if(currentTime - previousTime > timerSpawn){
        previousTime = currentTime;
        spawnItem(context);
    }
};

function spawnItem(context){
    let newCircle = new Polyphony.Shape(context, Polyphony.SHAPE.ELLIPSE, 20, 20, {backgroundColor: [100, 0, 0]});
    newCircle.coordinates.setX(Math.random()*250).setY(Math.random()*250);
    context.addToAllSelections(newCircle);
};

context.addSystem(SpawnSystem, Polyphony.EVENTS.ALL, 50)`;

let deleteSpawns = 
    `let timerSpawn = 1;
let lastSpawnTime = new Date().getTime() / 1000;
let trackedTime = new Date().getTime() / 1000;
let SpawnedItems = context.createSelection("Spawned", e => 'lifeTime' in e);

function SpawnSystem(context) {
    let currentTime = new Date().getTime() / 1000;
    if(currentTime - lastSpawnTime > timerSpawn){
        lastSpawnTime = currentTime;
        spawnItem(context);
    }
};

function spawnItem(context){
    let newCircle = new Polyphony.Shape(context, Polyphony.SHAPE.ELLIPSE, 20, 20, {backgroundColor: [100, 0, 0]});
    newCircle.coordinates.setX(Math.random()*250).setY(Math.random()*250);
    newCircle.lifeTime = 4;
    newCircle.speed = new Polyphony.Coordinates(32, -32, 0);
    context.addToAllSelections(newCircle);
};

function DespawnSystem(context){
    let currentTime = new Date().getTime() / 1000;
    let deltaTime = currentTime - trackedTime;
    trackedTime = currentTime;
    for(let e of SpawnedItems){
        e.lifeTime = e.lifeTime - deltaTime;
        if(e.lifeTime <= 0){
            e.toDelete = true;
            context.updateAllSelections(e);
        }
    }
};

context.addSystem(SpawnSystem, Polyphony.EVENTS.ALL, 50);
context.addSystem(DespawnSystem, Polyphony.EVENTS.ALL, 49);`;

let cannonSpawn = `let timerSpawn = 1000;
let lastSpawnTime = new Date().getTime();
let trackedTime = new Date().getTime();
let SpawnedItems = context.createSelection("Spawned", e => 'lifeTime' in e);
let SpeedingItems = context.createSelection("Speeding", e => 'speed' in e);

function SpawnSystem(context) {
    let currentTime = new Date().getTime();
    if(currentTime - lastSpawnTime > timerSpawn){
        lastSpawnTime = currentTime;
        spawnItem(context);
    }
};

function spawnItem(context){
    let newCircle = new Polyphony.Shape(context, Polyphony.SHAPE.ELLIPSE, 20, 20, {backgroundColor: [100, 0, 0]});
    newCircle.coordinates.setX(0).setY(50);
    newCircle.lifeTime = 4000;
    newCircle.speed = new Polyphony.Coordinates(50, 0, 0);
    context.addToAllSelections(newCircle);
};

function DespawnSystem(context){
    let currentTime = new Date().getTime();
    let deltaTime = currentTime - trackedTime;
    for(let e of SpawnedItems){
        e.lifeTime = e.lifeTime - deltaTime;
        if(e.lifeTime <= 0){
            e.toDelete = true;
            context.updateAllSelections(e);
        }
    }
};

function SpeedSystem(context){
  let deltaTime = new Date().getTime() - trackedTime;
  for(let e of context.selections["Speeding"]){
      let c = e.coordinates;
      e.coordinates.setX(c.x + e.speed.x * deltaTime / 1000).setY(c.y + e.speed.y * deltaTime / 1000);
    }
};

function TrackTime(context){
  let currentTime = new Date().getTime();    
  trackedTime = currentTime;
};

context.addSystem(SpawnSystem, Polyphony.EVENTS.ALL, 50);
context.addSystem(DespawnSystem, Polyphony.EVENTS.ALL, 49);
context.addSystem(SpeedSystem, Polyphony.EVENTS.ALL, 51);
context.addSystem(TrackTime, Polyphony.EVENTS.ALL, 60);`;

let gravityCannon = 
    `let timerSpawn = 1000;
let lastSpawnTime = new Date().getTime();
let trackedTime = new Date().getTime();
let SpawnedItems = context.createSelection("Spawned", e => 'lifeTime' in e);
let SpeedingItems = context.createSelection("Speeding", e => 'speed' in e);

function SpawnSystem(context) {
    let currentTime = new Date().getTime();
    if(currentTime - lastSpawnTime > timerSpawn){
        lastSpawnTime = currentTime;
        spawnItem(context);
    }
};

function spawnItem(context){
    let newCircle = new Polyphony.Shape(context, Polyphony.SHAPE.ELLIPSE, 20, 20, {backgroundColor: [100, 0, 0]});
    newCircle.coordinates.setX(0).setY(30);
    newCircle.lifeTime = 4000;
    newCircle.speed = new Polyphony.Coordinates(50, 0, 0);
    context.addToAllSelections(newCircle);
};

function DespawnSystem(context){
    let currentTime = new Date().getTime();
    let deltaTime = currentTime - trackedTime;
    for(let e of SpawnedItems){
        e.lifeTime = e.lifeTime - deltaTime;
        if(e.lifeTime <= 0){
            e.toDelete = true;
            context.updateAllSelections(e);
        }
    }
};

function SpeedSystem(context){
    let deltaTime = new Date().getTime() - trackedTime;
    for(let e of context.selections["Speeding"]){
        let c = e.coordinates;
        e.coordinates.setX(c.x + e.speed.x * deltaTime / 1000).setY(c.y + e.speed.y * deltaTime / 1000);
    }
};

function GravitySystem(context){
    let gravity = new Polyphony.Coordinates(0, 9.8, 0);
    let deltaTime = new Date().getTime() - trackedTime;
    for(let e of context.selections["Speeding"]){
        let s = e.speed;
        e.speed.setX(s.x + gravity.x * deltaTime / 1000).setY(s.y + gravity.y * deltaTime / 1000);
    }
};

function TrackTime(context){
    let currentTime = new Date().getTime();    
    trackedTime = currentTime;
};

context.addSystem(SpawnSystem, Polyphony.EVENTS.ALL, 50);
context.addSystem(DespawnSystem, Polyphony.EVENTS.ALL, 49);
context.addSystem(SpeedSystem, Polyphony.EVENTS.ALL, 51);
context.addSystem(GravitySystem, Polyphony.EVENTS.ALL, 52);
context.addSystem(TrackTime, Polyphony.EVENTS.ALL, 60);`;


export const codeExamples = { "squareExample": squareExample, "buttonExample": buttonExample, "lineExample": lineExample, "circleSpawner": circleSpawner, "deleteSpawns": deleteSpawns, "cannonSpawn": cannonSpawn, "gravityCannon": gravityCannon };