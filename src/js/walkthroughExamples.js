const tutoDebutExemple = {
    readOnlyRanges: [{ from: 1, to: 23 }, { from: -1, to: -1 }],
    content: `view.bounds.setW(1278).setH(1050);

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
};

for (let i = 0; i < 9; i++) {
    let xpos = i % 3;
    let ypos = Math.floor(i / 3);

    let square = Polyphony.Shape(
        context, 
        Polyphony.SHAPE.RECTANGLE, 
        50, 50, 
        {backgroundColor: [100, 0, 0]}
    );
    square.coordinates.setX(xpos * 100 + 100).setY(ypos * 100 + 100);

    if (i > 6)
        square.shape = Polyphony.SHAPE.ELLIPSE;

    context.addToAllSelections(square);
};

let MoveProperty = function MoveProperty(context) {
    for(let p of context.selections["Pointers"]){
			if(p.tmpReleased==0 && p.target 
               && p.target.shape == Polyphony.SHAPE.RECTANGLE){
				p.target.backgroundColor = [128 + getRandomInt(128), 
                                            128 + getRandomInt(128), 
                                            128 + getRandomInt(128)];    
			}
	}	  
}

context.addSystem(MoveProperty, Polyphony.EVENTS.POINTER_INPUT, 23);`};

const blabla = {
    readOnlyRanges: [{ from: 1, to: 1 }, { from: -3, to: -1 }],
    content: `blab
blabla
blalbabla
azzea
zeaea
aez` };

const sandbox = {
    readOnlyRanges: [],
    content: ``
};

export const walkthroughFiles = [tutoDebutExemple, blabla, sandbox];