/**
 * ECS base elements for polyphony.
 * @module PolyphonyECS
 */

/**
 * Setup Polyphony Context
 */

/**
 * Set of all Contexts.
 *
 * @type {{}}
 */
export let _all_contexts = {};
/**
 * Current context name.
 *
 * @type {string}
 */
export let _current_context_key = "default";
/**
 * Name for the default context.
 *
 * @type {string}
 */
export let _default_context_key = "default";


/**
 * Context of polyphony instance.
 *
 * @memberof module:PolyphonyECS
 * @class Context
 * @typedef {Context} Context
 */
class Context {
    /**
     * Flag to wait for renderer
     *
     * @type {boolean}
     */
    renderer_started = false;
    /**
     * Renderder instance
     *
     * @type {*}
     */
    renderer = null;
    
    /**
     * GUI element holding the canvas
     *
     * @type {*}
     */
    canvasElement = null;
    /**
     * Entity representing the canvas
     *
     * @type {Entity}
     */
    canvasEntity = null;

    /**
     * Set of all entities of the context
     *
     * @type {Set.<Entity>}
     */
    entities;
    /**
     * Array of all the selections of the context
     *
     * @type {Array<Set<Entity>>}
     */
    selections;


    /**
     * Creates an instance of Context.
     * 
     * @constructor
     * @param {string} contextName
     */
    constructor(contextName) {
        this.name = contextName;
        this.entities = new Set();
        this.updated = new Set();
        this.deleted = new Set();
        this.selections = {};
        _all_contexts[contextName] = this;
    };

    /**
     * Reset context.
     */
    clearContext() {
        this.entities.clear();
        this.updated.clear();
        this.deleted.clear();
        this.selections.length = 0;
    };

    /**
     * Base element, decorated by components describing it
     *
     * @typedef {Entity} Entity
     */

    /**
     * Create an Entity in the context.
     *
     * @param {{}} [dict={}]
     * @param {*} ext
     * @returns {Entity}
     */
    createEntity(dict = {}, ext) {
        if(ext !== undefined)
            Object.assign(dict, ext);

        this.entities.add(dict);
        return dict;
    };

    /**
     * Create a Selection in the context.
     *
     * @param {*} name
     * @param {*} descriptor
     * @param {*} compareFunction
     * @returns {Selection}
     */
    createSelection(name, descriptor, compareFunction) {
        let selection = new Array();
        selection.accept = descriptor;
        selection.compare = compareFunction;

        for (let entity of this.entities) {
            if (selection.accept(entity))
                selection.push(entity);
        }
        
        if(compareFunction) selection.sort(compareFunction);
        this.selections[name] = selection;
        return selection;
    };

    /**
     * Manage events to modify the devices impacted (pointer, keyboards, ...).
     *
     * @param {*} type
     * @param {*} event
     */
    manageEvent(type, event) {
        let metaType = getEventMetaType(type);
        let pointer = this.selections["Pointers"]?.[0];
        let keyboard = this.selections["Keyboards"]?.[0];

        if(pointer && metaType === EVENTS.POINTER_INPUT) 
            manageMouseEvent(pointer, type, event);
            
        if(keyboard && metaType === EVENTS.KEYBOARD_INPUT)
            manageKeyboardEvent(keyboard, type, event);
    };

    /**
     * Run every active Systems of the context triggered by an event.
     * 
     * @param {*} eventType
     */
    run(eventType) {
        if(!("Systems" in this.selections))
            return;

        for (let s of Array.from(this.selections["Systems"])) {
            if (s.runOn & eventType && !s.disabled)
                s(this);
        }
    };

    /**
     * Add an Entity to a Selection.
     * 
     * @param {Selection} selection
     * @param {Entity} entity
     */
    addToSelection(selection, entity) {
        if(Array.isArray(entity)){
            for(var e of entity){
                this.addToSelection(selection, e);
            }
        }

        if(selection.accept(entity)){
            selection.push(entity);
            sortSelection(selection);
        }
    };

    /**
     * Remove an Entity from a Selection.
     * 
     * @param {Selection} selection
     * @param {Entity} entity
     */
    removeFromSelection(selection, entity){ 
        if(Array.isArray(entity)){
            for(var e of entity){
                this.removeFromSelection(selection, e);
            }
        }

        let entityIndexInSelection = selection.indexOf(entity);
        if( entityIndexInSelection >= 0)
            selection.splice(entityIndexInSelection, 1);
    };

    /**
     * Update Selection with an Entity.
     * 
     * @param {Selection} selection
     * @param {Entity} entity
     */
    updateSelection(selection, entity){
        if(Array.isArray(entity)){
            for(var e of entity){
                this.updateSelection(selection, e);
            }
        }

        if(selection.accept(entity) && !selection.includes(entity)) {
            this.addToSelection(selection, entity)
        }
        else if(!selection.accept(entity) && selection.includes(entity)){
            this.removeFromSelection(selection, entity);
        }
        selection.sort(selection.compare);
    };

    /**
     * Add Entity to all of the Context Selections.
     *
     * @param {Entity} entity
     */
    addToAllSelections(entity) {
        for (let [name, selection] of Object.entries(this.selections)) {
            this.addToSelection(selection, entity);
        };
    };

    /**
     * Remove Entity from all of the Context Selections.
     *
     * @param {Entity} entity
     */
    removeFromAllSelections(entity){
        for (let [name, selection] of Object.entries(this.selections)) {
            this.removeFromSelection(selection, entity);
        };
    };

    /**
     * Update all Selections with an Entity.
     *
     * @param {Entity} entity
     */
    updateAllSelections(entity){
        for (let [name, selection] of Object.entries(this.selections)){
            this.updateSelection(selection, entity);
        };
    };

    /**
     * Add a System to the Context.
     *
     * @param {function} system
     * @param {EVENTS} runEvent
     * @param {number} runOrder
     * @param {*} data
     */
    addSystem(system, runEvent, runOrder, data){
        let systemToEntity = this.createEntity(
            system,
            {runOn: runEvent, order: runOrder, data: data}
        );
        this.addToAllSelections(systemToEntity);
        sortSelection(this.selections["Systems"]);
    };

    /**
     * Get a System reference from the context.
     *
     * @param {string} systemName
     * @returns {Entity}
     */
    getSystem(systemName){
        for(let system of this.selections["Systems"]){
            if(system.name == systemName)
                return system;
        }
        throw new Error("Unregistered system: " + systemName);
    };

    /**
     * Set canvas element to given element.
     *
     * @param {*} canvasElement
     */
    setCanvas(canvasElement) {
        switch(canvasElement.tagName.toLowerCase()){
            case "svg":
                this.setSVGRendering();
                break;
            case "canvas":
                this.setCanvasRendering();
                break;
            default:
                throw new Error('Unrecognized type: ' + type);      
        }
        this.canvasElement = canvasElement;
    };  

    /**
     * Set rendering to SVG system.
     */
    setSVGRendering() {
        import('./renderSVG.js')
        .then(obj => {
            this.setRenderer(obj);
            this.addSystem(this.renderer.DependencyChangePropagation, EVENTS.DISPLAY_OUTPUT, 79);
        })
        .catch(err => console.log(err))
    };

    /**
     * Set rendering to Canvas system.
     */
    setCanvasRendering() {
        import('./renderCanvas.js')
        .then(obj => this.setRenderer(obj))
        .catch(err => console.log(err));
    };
    
    /**
     * Set renderer to the given instance.
     *
     * @param {*} renderer_instance
     */
    setRenderer(renderer_instance) {
        this.renderer = renderer_instance;
        this.addSystem(this.renderer.SoftRenderSystem, EVENTS.DISPLAY_OUTPUT, 80);
        console.log("renderer loaded");
    };

    /**
     * Start the render loop.
     */
    startRendering() {
        if (this.renderer == null) {
            setTimeout(() => { this.startRendering(); }, 50);
            return;
        }

        if(this.renderer){
                this.renderer.initCanvas(this.canvasElement, this.canvasEntity.bounds.width, this.canvasEntity.bounds.height);
                this.renderer.initRender();
                this.renderer_started = true;
        } 
        else {
            throw new Error('Impossible to start the rendering: no renderer set');
        }
    };
};


/**
 * Impact event on mouse parameters.
 *
 * @param {Entity} pointer
 * @param {*} type
 * @param {*} event
 */
let manageMouseEvent = function(pointer, type, event){
    switch (type) {
        case "mousedown": {
            let b = event.button;
            pointer.buttons |= 1 << b;
            pointer.tmpPressed = b;
        } break;
        case "mouseup": {
            let b = event.button;
            pointer.buttons &= ~(1 << b);
            pointer.tmpReleased = b;
        } break;
        case "mousemove": {
            let pos = pointer.coordinates;
            pointer.tmpMotion = new Coordinates(pos.x - event.clientX, pos.y - event.clientY);
            pos.x = event.clientX;
            pos.y = event.clientY;
        } break;
        case "mousedb": {
            let b = event.button;
            pointer.tmpDB = true;
        } break;
        case "wheel": {
            pointer.tmpWheel = new Coordinates(event.deltaX, event.deltaY);
        } break;
        default: {
            console.log("Unexpected mouse event " + type + " " + event);
        }
    };
};

/**
 * Impact event on keyboard parameters.
 *
 * @param {Entity} keyboard
 * @param {*} type
 * @param {*} event
 */
let manageKeyboardEvent = function(keyboard, type, event){
    switch (type) {
        case "keydown": {
            let c = event.code;
            keyboard.tmpPressed = c;
            keyboard.keyStates.add(c);
            keyboard.tmpSymbol = c;
            if ((event.keyCode >= 32 && event.keyCode <= 111) || (event.keyCode >= 169 && event.keyCode <= 173))
                keyboard.input += event.key;
        } break;
        case "keyup": {
            let c = event.code;
            keyboard.tmpReleased = c;
            keyboard.keyStates.delete(c);
        } break;
        default: {
            console.log("Unexpected keyboard event " + type + " " + event);
        }
    };
};

/**
 * Sort selection by its comparison criteria.
 *
 * @param {*} selection
 */
let sortSelection = function(selection){
    if(selection.compare)
        selection.sort(selection.compare);
}

/**
 * Set current Context to contextName.
 *
 * @param {string} contextName
 */
let setContext = function (contextName) {
    _current_context_key = contextName;
};

/**
 * Get Context defined by contextName.
 *
 * @param {string} contextName
 * @returns {Context}
 */
let getContext = function(contextName) {
    if (contextName)
        return _all_contexts[contextName];
    return _all_contexts[_current_context_key];
};

/**
 * Reset Context (will remove all entities, selections and systems from context).
 *
 * @param {string} contextName
 */
let clearContext = function (contextName) {
    if (contextName)
        _all_contexts[contextName].entities.clear();
    else
        _all_contexts[_current_context_key].entities.clear();
};

export { Context, setContext, getContext, clearContext };

/**
 * Event const - Flag for event the systems can react to.
 * @readonly
 * @enum {number}
 * @type {number}
 */
const EVENTS = {
    /** Mouse and other related pointers events */    
    POINTER_INPUT: 1,
    /** Keyboard related events */
    KEYBOARD_INPUT: 2,
    /** Screen refresh events */
    DISPLAY_OUTPUT: 4,
    /** All events at once */
    ALL: 7
};

/**
 * Meta System - System responsible for calling other Systems on each context.
 *
 * @param {EVENTS} metaType
 * @param {*} event
 */
let MetaSystem = function MetaSystem(metaType, event) {
    Object.values(_all_contexts).forEach(context => {
        if(event)
            context.manageEvent(event.type, event.event);
        
        if(!context.renderer_started)
            metaType ^= EVENTS.DISPLAY_OUTPUT;
    
        context.run(metaType);
    });
};


/**
 * End of ECS base elements
 */

export { EVENTS, MetaSystem }

/** 
 * Polyphony Components
 * @namespace module:PolyphonyECS.Components
 */

/**
 * Coordinates component.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {number} [x=0]
 * @param {number} [y=0]
 * @param {number} [z=0]
 * @returns {Coordinates}
 */
let Coordinates = function Coordinates(x = 0, y = 0, z = 0) {
    this.x = x;
    this.y = y;
    this.z = z;
};
/**
 * Distance between this and another Coordinates component.
 * 
 * @method
 * @param {Coordinates} c 
 * @returns {number}
 */
Coordinates.prototype.distance = function (c) { return Math.hypot(this.x - c.x, this.y - c.y, this.z - c.z) };
/**
 * Set X coordinate.
 * 
 * @method
 * @param {number} x 
 * @returns {void}
 */
Coordinates.prototype.setX = function (x) { this.x = x; return this };
/**
 * Set Y coordinate.
 * 
 * @method
 * @param {number} y 
 * @returns {void}
 */
Coordinates.prototype.setY = function (y) { this.y = y; return this };
/**
 * Move coordinates to given position.
 * 
 * @method
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
Coordinates.prototype.moveTo = function (x, y) {
    this.x = x;
    this.y = y;
    this.has_changed = true;
    return this;
};
/**
 * Move coordinates by given units.
 * 
 * @method
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
Coordinates.prototype.moveBy = function (x, y) {
    this.x += x;
    this.y += y;
    this.has_changed = true;
    return this;
};
/**
 * Angle made by this and the Coordinates component, on the x axis.
 * Returns a positive angle between 0 and 360. 
 * 
 * @method
 * @param {Coordinates} coordinates 
 * @returns {number}
 */
Coordinates.prototype.angleBoundsDegrees = function (coordinates) {
    let directionRayX = this.x - coordinates.x;
    let directionRayY = this.y - coordinates.y;
    let degreeAngle = Math.atan2(directionRayY, directionRayX) * 180 / Math.PI;

    return (degreeAngle + 360) % 360;
};

/**
 * Coordinates component for line objects (with a start and an end).
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 * @returns {Coordinates}
 */
let LineCoordinates = function Line(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
};
/**
 * Set start coordinate for the line.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
LineCoordinates.prototype.setStart = function (x, y) { this.x1 = x; this.y1 = y; this.has_changed = true; return this };
/**
 * Set start coordinate for the line.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
LineCoordinates.prototype.setEnd = function (x, y) { this.x2 = x; this.y2 = y; this.has_changed = true; return this };
/**
 * Move coordinates to given position.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
LineCoordinates.prototype.moveTo = function (x, y) {
    let offsetX = x - this.x;
    let offsetY = y - this.y;
    this.x1 += offsetX; this.x2 += offsetX;
    this.y1 += offsetY; this.y2 += offsetY;
    this.has_changed = true;
    return this;
};
/**
 * Move coordinates by given units.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
LineCoordinates.prototype.moveBy = function (x, y) {
    this.x1 += x; this.x2 += x;
    this.y1 += y; this.y2 += y;
    this.has_changed = true;
    return this;
};
/**
 * Grow line along Bounds width.
 * 
 * @method
 * 
 * @param {number} x 
 */
LineCoordinates.prototype.growW = function (x) {
    let offset = (this.x1 < this.x2) ? x / 2 : -x / 2;
    this.x1 -= offset;
    this.x2 += offset;
};
/**
 * Grow line along Bounds height.
 * 
 * @method
 * 
 * @param {number} y 
 */
LineCoordinates.prototype.growH = function (y) {
    let offset = (this.y1 < this.y2) ? y / 2 : -y / 2;
    this.y1 -= offset;
    this.y2 += offset;
};
/**
 * @property {number}
 */
Object.defineProperty(LineCoordinates.prototype, "x", { get() {return Math.min(this.x1, this.x2) } });
/**
 * @property {number}
 */
Object.defineProperty(LineCoordinates.prototype, "y", { get() {return Math.min(this.y1, this.y2) } });
/**
 * @property {number}
 */
Object.defineProperty(LineCoordinates.prototype, "z", { get() {return Math.min(this.z1, this.z2) } });

/**
 * Coordinates component for line defined by two Entities.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {Entity} startEntity
 * @param {Entity} endEntity
 * @returns {Coordinates}
 */
let LinkLineCoordinates = function(startEntity, endEntity){
    this.startEntity = startEntity;
    this.endEntity = endEntity;
};
/**
 * Set position of start Entity.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
LinkLineCoordinates.prototype.setStart = function(x, y){ this.startEntity.coordinates.x = x, this.startEntity.coordinates.y = y; this.has_changed = true; return this};
/**
 * Set position of end Entity.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 * @returns {void}
 */
LinkLineCoordinates.prototype.setEnd = function(x, y){ this.endEntity.coordinates.x = x, this.endEntity.coordinates.y = y; this.has_changed = true; return this};
/**
 * Move composing Entities to move coordinates to given position.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 */
LinkLineCoordinates.prototype.moveTo = function(x, y){
    let positionX = this.x;
    let positionY = this.y;
    
    this.startEntity.coordinates.x = this.startEntity.coordinates.x - positionX + x;
    this.startEntity.coordinates.y = this.startEntity.coordinates.y - positionY + y;
    
    this.endEntity.coordinates.x = this.endEntity.coordinates.x - positionX + x;
    this.endEntity.coordinates.y = this.endEntity.coordinates.y - positionY + y;
};
/**
 * Move coordinates Entities to move coordinates by given units.
 * 
 * @method
 * 
 * @param {number} x 
 * @param {number} y 
 */
LinkLineCoordinates.prototype.moveBy = function(x, y){
    this.startEntity.coordinates.x += x;
    this.startEntity.coordinates.y += y;
    
    this.endEntity.coordinates.x += x;
    this.endEntity.coordinates.y += y;
};
/**
 * Grow line along Bounds width.
 * 
 * @method
 * 
 * @param {number} x 
 */
LinkLineCoordinates.prototype.growW = function(x){
    let offset = (this.startEntity.coordinates.x < this.endEntity.coordinates.x) ? x / 2 : -x / 2;
    this.startEntity.coordinates.x -= offset;
    this.endEntity.coordinates.x += offset;
};
/**
 * Grow line along Bounds height.
 * 
 * @method
 * 
 * @param {number} y 
 */
LinkLineCoordinates.prototype.growH = function(y){
    let offset = (this.startEntity.coordinates.y < this.endEntity.coordinates.y) ? y / 2 : -y / 2;
    this.startEntity.coordinates.y -= offset;
    this.endEntity.coordinates.y += offset;
};
Object.defineProperty(LinkLineCoordinates.prototype, "x1", { get() {return this.startEntity.coordinates.x}})
Object.defineProperty(LinkLineCoordinates.prototype, "y1", { get() {return this.startEntity.coordinates.y}})
Object.defineProperty(LinkLineCoordinates.prototype, "x2", { get() {return this.endEntity.coordinates.x}})
Object.defineProperty(LinkLineCoordinates.prototype, "y2", { get() {return this.endEntity.coordinates.y}})
Object.defineProperty(LinkLineCoordinates.prototype, "x", { get() {return Math.min(this.startEntity.coordinates.x, this.endEntity.coordinates.x) } });
Object.defineProperty(LinkLineCoordinates.prototype, "y", { get() {return Math.min(this.startEntity.coordinates.y, this.endEntity.coordinates.y) } });
Object.defineProperty(LinkLineCoordinates.prototype, "z", { get() {return Math.min(this.startEntity.coordinates.z, this.endEntity.coordinates.z) } });



/**
 * Bounds component.
 * Bounding Box for every component, defined by its width and height.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {number} width
 * @param {number} height
 * @returns {Bounds}
 */
let Bounds = function Bounds(width, height) {
    this.width = width;
    this.height = height;
};
/**
 * Set width.
 * 
 * @method
 * 
 * @param {*} width 
 * @returns {void}
 */
Bounds.prototype.setW = function (width) { this.width = width; this.has_changed = true; return this };
/**
 * Set height.
 * 
 * @method
 * 
 * @param {*} height 
 * @returns {void}
 */
Bounds.prototype.setH = function (height) { this.height = height; this.has_changed = true; return this };
/**
 * Check if Coordinates are contained in the Entity's Bounds.
 * 
 * @method
 * 
 * @static
 * @param {Entity} entity 
 * @param {Coordinates} coordinates 
 * @returns {boolean}
 */
Bounds.contains = function (entity, coordinates) { 
    return (entity.coordinates.x) <= coordinates.x 
            && coordinates.x <= (entity.coordinates.x + entity.bounds.width)
            && (entity.coordinates.y) <= coordinates.y 
            && coordinates.y <= (entity.coordinates.y + entity.bounds.height);
}; 

/**
 * Linked Line Bounds.
 * Bounds for dynamic line, where size is defined by two Entities.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {Entity} startEntity
 * @param {Entity} endEntity
 * @returns {Bounds}
 */
let LinkLineBounds = function(startEntity, endEntity){
    this.startEntity = startEntity;
    this.endEntity = endEntity;
};
LinkLineBounds.prototype.setW = function(width){ return this };
LinkLineBounds.prototype.setH = function(width){ return this };
Object.defineProperty(LinkLineBounds.prototype, "width", { get() {return Math.abs(this.startEntity.coordinates.x - this.endEntity.coordinates.x) } });
Object.defineProperty(LinkLineBounds.prototype, "height", { get() {return Math.abs(this.startEntity.coordinates.y - this.endEntity.coordinates.y) } });


/**
 * Border component.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {*} color
 * @param {number} [width=1]
 * @returns {Border}
 */
let Border = function Border(color, width = 1) {
    this.color = color;
    this.width = width;
};

/**
 * Image component.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {*} src
 * @param {} width
 * @param {} height
 * @param {number} [opacity=1]
 * @param {number} [paddingW=0]
 * @param {number} [paddingH=0]
 * @returns {Image}
 */
let Image = function Image(src, width, height, opacity = 1, paddingW = 0, paddingH = 0) {
    this.src = src;
    this.canvasSrc = document.createElement('img');
    this.canvasSrc.src = src;
    this.opacity = opacity;
    this.paddingW = paddingW;
    this.paddingH = paddingH;
    this.width = width;
    this.height = height;
};

/**
 * Glyph component.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {*} font
 * @param {*} code
 * @returns {Glyph}
 */
let Glyph = function Glyph(font, code) {
    this.font = font;
    this.code = code;
};

/**
 * Font component.
 * 
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {*} file
 * @param {*} size
 * @param {*} style
 * @param {*} color
 * @returns {Font}
 */
let Font = function Font(file, size, style, color) {
    this.file = file;
    this.size = size;
    this.style = style;
    this.color = color;
};

/**
 * RichText Component.
 * @class 
 * @memberof module:PolyphonyECS.Components
 * 
 * @param {*} str
 * @param {*} fonts
 * @param {number} [size=1]
 * @param {number} [paddingW=0]
 * @param {number} [paddingH=0]
 * @param {number} [indent=4]
 * @param {boolean} [editable=false]
 * @param {*} focusStyle
 * @returns {RichText}
 */
let RichText = function RichText(str, fonts, size=1, paddingW = 0, paddingH = 0, indent = 4, editable = false, focusStyle) {
    this.str = str;
    this.fonts = fonts instanceof Font ? { 0: fonts } : fonts;
    this.paddingW = paddingW;
    this.paddingH = paddingH;
    this.width = str.length * size + paddingW * 2;
    this.height = size + paddingH * 2;
    this.indent = indent;
    this.editable = editable;
    this.focused = false;
    this.focusStyle = focusStyle;
    this._focusStyle = {};
    Object.defineProperty(this, 'texture', { value: null, writable: true });
};
/**
 * Add text to component
 * 
 * @memberof module:PolyphonyECS.RichText
 * @method
 * 
 * @param {*} s 
 * @returns {void}
 */
RichText.prototype.addStr = function (s) { this.str += s; return this; };

export {
    Coordinates, LineCoordinates, Bounds, Border,
    Image, Glyph, Font, RichText,
}

/**
 * Systems to handle interface behaviours
 * @namespace module:PolyphonyECS.System
 */

/**
 * Pointer target - for each Pointer, assigns target to be the topmost targetable Entity that contains the cursor.
 *
 * @export
 * @function module:PolyphonyECS.System.PointerTargetSystem
 * @param {Context} context
 */
export function PointerTargetSystem(context) {
    function pick(x, y, sel) {
        for (let e of sel) {
            if (Bounds.contains(e, new Coordinates(x, y, 0)))
                return e;
        }
    }
    
    for (let pointer of context.selections["Pointers"]) {
        sortSelection(context.selections["Systems"]);
        let x = pointer.coordinates.x;
        let y = pointer.coordinates.y;
        let view = pick(x, y, context.selections["Views"]);
        let _view = pointer.view;
        if (view !== _view)
            pointer.view = view;
        if (!view)
            continue;
        x += view.origin.x;
        y += view.origin.y;
        let target = pick(x, y, context.selections["Targetables"]);
        let _target = pointer.target;
        if (target !== _target)
            pointer.target = target;
    }
};

/**
 * Pointer click - generates a click when pressing&releasing buttons on the same target and not too far apart.
 *
 * @export
 * @function module:PolyphonyECS.System.PointerClickSystem
 * @param {Context} context
 */
export function PointerClickSystem(context) {
    for (let p of context.selections["Pointers"]) {
        let pos = p.coordinates
        let target = p.target
        let leftClickOnValidTarget = p.tmpReleased === 0 
            && target && target.clickable && target === p.pressTarget 
            && pos.distance(p.pressPosition) < 10;

        if (p.tmpPressed === 0) {
            p.pressPosition = new Coordinates(pos.x, pos.y)
            p.pressTarget = target
        } 
        else if (leftClickOnValidTarget) {
            p.tmpClick = target
            if (target !== undefined)
                target.tmpClickBy = p
        }
    }
};

/**
 * Pointer drag - implements drag&drop interaction technique.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.PointerDragSystem
 */
export function PointerDragSystem(context) {
    for (let pointer of context.selections["Pointers"]) {
        let cursorPos = pointer.coordinates
        let target = pointer.pressTarget
        let moveValidTarget = !pointer.dragging && 
            pointer.buttons & 1 && 
            target && target.draggable && 
            cursorPos.distance(pointer.pressPosition) > 10;
            
        if (moveValidTarget) {
            target.draggedBy = pointer
            pointer.dragging = target
            delete target.targetable // cannot be "seen" by any other pointer
        }

        let dragging = pointer.dragging // Entity being dragged
        if (dragging) {
            dragging.coordinates.moveTo(cursorPos.x, cursorPos.y) // signal Component update to Polyphony

            if (pointer.tmpReleased === 0) {
                delete pointer.dragging
                delete dragging.draggedBy
                pointer.tmpDropped = dragging
                dragging.targetable = true
                if (pointer.target)
                    pointer.target.tmpDropOf = dragging
            }
        }
    }
};

/**
 * Toggle buttons - detects clicks on Togglable buttons, and untoggles other buttons with the same toggleGroup.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.ToggleSystem
 */
export function ToggleSystem(context) {
    for (let pointer of context.selections["Pointers"]) {
        let target = pointer.tmpClick;

        if (!target || !context.selections["Togglables"].accept(target))
            continue;
        
        let group = target.toggleGroup;
        if (group !== null && target.toggleCount > 0) for (let toggle of context.selections["Togglables"]) {
            if (toggle.toggled && toggle.toggleGroup === group) {
                toggle.toggled += 1;

                if(toggle.toggled > toggle.toggleCount){
                    deactivateToggle(toggle);
                    break;
                }
            }
        }

        if(target.toggled && target.toggleCount == 0)
            deactivateToggle(target);
        else
            activateToggle(target);
    }
};

/**
 * Activate toggle style
 *
 * @param {*} toggle
 */
let activateToggle = function(toggle) {
    toggle.toggled = 1;
    toggle.bounds.has_changed = true;
    let style = toggle.toggleStyle;
    let old = toggle._toggleStyle || (toggle._toggleStyle = {});
    ReplaceComponentsInEntity(toggle, style, old);
}

/**
 * Deactivate toggle style
 *
 * @param {*} toggle
 */
let deactivateToggle = function(toggle) {
    delete toggle.toggled;        
    toggle.bounds.has_changed = true;
    let old = toggle._toggleStyle;
    DeleteUndefinedComponents(toggle, old);
    ReplaceComponentsInEntity(toggle, old);
}


/**
 * Hover System - Detects a pointer on an hoverable component and activates it.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.HoverSystem
 */
export function HoverSystem(context) {
	for (let pointer of context.selections["Pointers"]) {
		let target = pointer.target;
		if (!target || !context.selections["Hoverables"].accept(target)){
            for(let hoverable of context.selections["Hoverables"]){
                if(hoverable.hovered){
                    ReplaceComponentsInEntity(hoverable, hoverable._hoverStyle, hoverable.hoverStyle);
                    delete hoverable._hoverStyle;
                    delete hoverable.hovered;
                }
            }
            continue;
        }
			
		let group = target.hoverGroup;
		if (group !== null) for (let entity of context.selections["Hoverables"]) {
			if (entity.hovered && entity.hoverGroup === group) {
				delete entity.hovered;
				let old = entity._hoverStyle;
                DeleteUndefinedComponents(entity, old);
                ReplaceComponentsInEntity(entity, old);
				break;
			};
		};

		target.hovered = true;
		let style = target.hoverStyle;
		let old = target._hoverStyle || (target._hoverStyle = {});
        ReplaceComponentsInEntity(target, style, old);
	}
};

/**
 * Switch components on an entity using a style, and optionnaly storing the replaced components values.
 *
 * @param {Entity} entity
 * @param {*} alternativeComponent
 * @param {*} [storageComponent=null]
 */
export let ReplaceComponentsInEntity = function(entity, alternativeComponent, storageComponent = null){
    for(let component in alternativeComponent){
        if(storageComponent !== null)
            storageComponent[component] = entity[component];
        entity[component] = alternativeComponent[component];
    }
    Object.assign(entity, alternativeComponent);
}

/**
 * Delete undefined components in an entity, based on a set of components.
 *
 * @param {Entity} entity
 * @param {*} componentsToDelete
 */
export let DeleteUndefinedComponents = function(entity, componentsToDelete) {
    for(let component in componentsToDelete){
        if(componentsToDelete[component] === null)
            delete entity[component];
    }
}


/**
 * Focus - Trigger the Focus on an entity when clicking on a focusable element.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.FocusSystem
 */
export function FocusSystem(context) {
	// detect mouse presses on a focusable Entity
	let focus = undefined;
	for (let pointer of context.selections["Pointers"]) {
		let target = pointer.target;
        let pressOnFocusable = pointer.tmpPressed !== undefined && 
            target && target.richText && 
            target.richText.editable

		if (pressOnFocusable)
			focus = target;
	}
	
	if (focus) {
		// for each keyboard, unfocus the previously focused Entity
		for (let keyboard of context.selections["Keyboards"]) {
			changeFocusedEntity(keyboard, focus);
		}
		
		// make the new Entity visually focused
		let target = focus.richText;
		if (target) {
			target.focused = true;
			let style = target.focusStyle;
			let old_style = target._focusStyle || (target._focusStyle = {});
            ReplaceComponentsInEntity(focus, style, old_style);
		}
	}
};

/**
 * Switch focus style activation from one entity to another.
 *
 * @param {*} device
 * @param {*} newFocus
 */
export let changeFocusedEntity = function(device, newFocus){
    let old_focus = device.focus;
    device.focus = newFocus;
    if (!old_focus || !old_focus.richText)
        return;
    let richText = old_focus.richText;
    richText.focused = false;
    old_focus.bounds.has_changed = true;
    let old_style = richText._focusStyle;
    DeleteUndefinedComponents(old_focus, old_style);
    ReplaceComponentsInEntity(old_focus, old_style);
};

/**
 * Text Edition - Send typed text when writing.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.TextEditSystem
 */
export function TextEditSystem(context) {
    // send typed characters to each currently focused Entity
    for (let keyboard of context.selections["Keyboards"]) {
        let text = keyboard.targetTextfield;
        let focus = keyboard.focus;
        if (text && focus) {
            focus.richText = focus.richText.addStr(keyboard.input);
            focus.bounds.has_changed = true;
        }
        keyboard.input = "";
    }
};

/**
 * Shortcuts - detects and signals key presses while holding CTRL/CMD key.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.ShortcutSystem
 */
export function ShortcutSystem(context) {
    let s = context.selections["Keyboards"][0].tmpSymbol
    if (s && (context.selections["Keyboards"][0].keyStates.has("ControlLeft") || context.selections["Keyboards"][0].keyStates.has("ControlRight")))
        context.selections["Keyboards"][0].tmpShortcut = s
};

/**
 * Delete temporary Components - clears any Components with tmp prefix, after all Systems have executed.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.DeleteTmpSystem
 */
export function DeleteTmpSystem(context){
    for (let e of context.entities) {
        for (let property in e) {
            if (property.startsWith('tmp'))
                delete e[property]
        }
    }
};

/**
 * Delete Entity - delete all entities tagged with a toDelete component.
 *
 * @export
 * @param {Context} context
 * @function module:PolyphonyECS.System.DeleteSystem
 */
export function DeleteSystem(context) {    
    for (let e of context.selections["Deletables"]) {
        context.entities.delete(e);
        context.removeFromAllSelections(e);
    }
};

/**
 * Description placeholder
 *
 * @type {*}
 */
let font;

/**
 * Factories - take an existing Entity (or make a new one), and augment it to acquire a given behaviour.
 * @namespace module:PolyphonyECS.Factories
 */

/**
 * Factory to create a Button.
 * 
 * @function module:PolyphonyECS.Factories.Button
 * @param {Context} context
 * @param {*} imgOrTxt
 * @param {*} x
 * @param {*} y
 * @param {Entity} [entity=undefined]
 * @returns {Entity}
 */
let Button = function (context, imgOrTxt, x, y, entity = undefined) {
    entity = entity || {};
    entity.depth = entity.depth || 0;
    entity.bounds = entity.bounds || new Bounds(imgOrTxt.width + 8, imgOrTxt.height + 8);
    entity.coordinates = new Coordinates(x, y, 0);
    entity.shape = entity.shape || SHAPE.RECTANGLE;
    entity.backgroundColor = entity.backgroundColor || null;
    
    entity.targetable = true;
    entity.clickable = true;
    if (imgOrTxt instanceof Image) {
        entity.image = imgOrTxt;
        imgOrTxt.paddingW = imgOrTxt.paddingH = 4;
    } else {
        entity.richText = imgOrTxt;
    }
    return context.createEntity(entity);
}

/**
 * Factory to create a Canvas.
 *
 * @function module:PolyphonyECS.Factories.Canvas
 * @param {Context} context
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {Entity} [entity=undefined]
 * @returns {Entity}
 */
let Canvas = function (context, x, y, width, height, entity = undefined) {
    entity = entity || {};
    entity.depth = entity.depth || 1;
    entity.bounds = new Bounds(width, height);
    entity.coordinates = new Coordinates(x, y, 0);
    entity.shape = entity.shape || SHAPE.RECTANGLE;
    entity.targetable = true;
    entity.children = entity.children || [];
    return context.createEntity(entity);
}

/**
 * Factory to create a CheckBox Item.
 *
 * @function module:PolyphonyECS.Factories.CheckBox
 * @param {Context} context
 * @param {*} imgOrTxt
 * @param {*} x
 * @param {*} y
 * @param {*} group
 * @param {*} style
 * @param {Entity} [entity=undefined]
 * @returns {Entity}
 */
let CheckBox = function(context, imgOrTxt, x, y, group, style, entity = undefined){
    entity = new Button(context, imgOrTxt, x, y, entity);
    entity.toggleCount = 0;
    entity.toggleGroup = group || null;
    entity.toggleStyle = style || null;

    return context.createEntity(entity);
}

/**
 * Factory to create a Radio Item.
 *
 * @function module:PolyphonyECS.Factories.RadioButton
 * @param {Context} context
 * @param {*} imgOrTxt
 * @param {*} x
 * @param {*} y
 * @param {*} group
 * @param {*} style
 * @param {Entity} [entity=undefined]
 * @returns {Entity}
 */
let RadioButton = function(context, imgOrTxt, x, y, group, style, entity = undefined){
    entity = new Button(context, imgOrTxt, x, y, entity);   
    entity.toggleCount = 1;
    entity.toggleGroup = group || null;
    entity.toggleStyle = style || null;    

    return context.createEntity(entity);
}

/**
 * Shape const - Flag for the kind of shape.
 *
 * @readonly
 * @enum {number}
 * @type {number}
 */
const SHAPE = {
    /** Rectangles */
    RECTANGLE: 0,
    /** Ellipses */
    ELLIPSE: 1,
    /** Lines */
    LINE: 2,
    /** Complex shapes */
    POLYLINE: 3
}

/**
 * Factory to create a Shape defined in SHAPE.
 *
 * @function module:PolyphonyECS.Factories.Shape
 * @param {Context} context
 * @param {*} type
 * @param {*} width
 * @param {*} height
 * @param {Entity} [entity=undefined]
 * @returns {Entity}
 */
let Shape = function (context, type, width, height, entity = undefined) {
    entity = entity || {};
    entity.depth = entity.depth || 0;
    entity.coordinates = entity.coordinates || new Coordinates();
    entity.bounds = entity.bounds || new Bounds(width, height);
    entity.shape = type;
    entity.targetable = entity.targetable || true;
    entity.draggable = entity.draggable || true;
    return context.createEntity(entity);
}

/**
 * Find extrema values in a point cloud
 *
 * @param {*} points
 * @returns {{}}
 */
let computeExtrema2D = function(points){
    if(points.length == 0)
        return undefined;

    let [minX, minY] = points[0];
    let [maxX, maxY] = points[0];

    for(let point of points){
        minX = Math.min(minX, point[0]);
        minY = Math.min(minY, point[1]);
        maxX = Math.max(maxX, point[0]);
        maxY = Math.max(maxY, point[1]);
    }

    return [minX, maxX, minY, maxY];
}

/**
 * Compute the Bounds of a 2D Shape
 *
 * @param {*} points
 * @returns {Bounds}
 */
let computeBound2D = function(points){
    let [minX, maxX, minY, maxY] = computeExtrema2D(points);
    return new Bounds(Math.abs(maxX - minX), Math.abs(maxY - minY));
}

/**
 * Compute center of a 2D Shape Bounding Box
 *
 * @param {*} points
 * @returns {Coordinates}
 */
let computeCenter2D = function(points){
    let [minX, maxX, minY, maxY] = computeExtrema2D(points);
    return new Coordinates( (maxX + minX) / 2, (maxY + minY) / 2, 0);
}

/**
 * Factory for non defined Shapes
 *
 * @function module:PolyphonyECS.Factories.CustomShape
 * @param {Context} context
 * @param {*} points
 * @param {Entity} [entity=undefined]
 * @returns {Entity}
 */
let CustomShape = function(context, points, entity=undefined) {
    let centeredPoints = [];
    let customBounds = computeBound2D(points);
    let customCoordinates = computeCenter2D(points);

    entity = Shape(context, SHAPE.POLYLINE, customBounds.width, customBounds.height, entity);

    for(let i = 0; i < points.length; i++){
        centeredPoints[i] = [ points[i][0] - customCoordinates.x, points[i][1] - customCoordinates.y ];
    }

    entity.points = centeredPoints;
    entity.coordinates = customCoordinates;

    return context.createEntity(entity);  
}

/**
 * Factory for a Line defined by two Entities
 *
 * @function module:PolyphonyECS.Factories.LinkLine
 * @param {Context} context
 * @param {Entity} startEntity
 * @param {Entity} endEntity
 * @param {Entity} [entity=undefined]
 * @returns {Entity}
 */
let LinkLine = function(context, startEntity, endEntity, entity = undefined){
    entity = entity || {};
    entity.coordinates = new LinkLineCoordinates(startEntity, endEntity);
    entity.bounds = new LinkLineBounds(startEntity, endEntity);    
    
    addDependency(startEntity, entity);
    addDependency(endEntity, entity);

    return Shape(context, SHAPE.LINE, 0, 0, entity);
};

let addDependency = function(entity, dependency){
    if(entity.dependency)
        entity.dependency.add(dependency);
    else
        entity.dependency = [ dependency ];
}

export {
    SHAPE,
    Button, RadioButton, CheckBox, Canvas, Shape, CustomShape, LinkLine, addDependency,
    computeBound2D, computeCenter2D
}


/*
 * Gestion des �v�nements
 **/

/**
 * Description placeholder *
 * @type {{}}
 */
let eventStack = []
/**
 * Description placeholder *
 * @type {*}
 */
let event;

/**
 * Description placeholder *
 * @param {*} eventType
 * @returns {number}
 */
let getEventMetaType = function(eventType){
    let metaType = 0;
    switch(eventType){
        case "mousedown":
        case "mouseup":
        case "mousemove":
        case "mousedb":
        case "wheel":
            metaType |= EVENTS.POINTER_INPUT;
            break;
        case "keyup":
        case "keydown" : 
            metaType |= EVENTS.KEYBOARD_INPUT;
            break;
        default:
            break;      
    };
    return metaType;
};

/**
 * Description placeholder
 */
let mainLoop = function () {
    while (eventStack.length > 0) {
        event = eventStack.shift();
        let metaType = getEventMetaType(event.type);
        if (metaType) {
            metaType |= EVENTS.DISPLAY_OUTPUT;
            MetaSystem(metaType, event);
        }
    }
    MetaSystem(EVENTS.DISPLAY_OUTPUT, null);
};

/**
 * Description placeholder
 */
let startPolyphony = function(){
    setInterval(mainLoop, 25);
};

export { eventStack, getEventMetaType, mainLoop, startPolyphony }


/**
 * Base Entities
 */

/**
 * Add View, Pointer and Keyboard Entities to the Context
 * 
 * @param {Context} context
 */
export let addViewPointerKeyboardToContext = function(context){
    let view = context.createEntity({ name: 'view', bounds: new Bounds(1024, 768), depth: 1, origin: new Coordinates(), coordinates: new Coordinates(0, 0) })
    let pointer = context.createEntity({ name: 'mouse', coordinates: new Coordinates(), pointerId: 0, buttons: 0 })
    let keyboard = context.createEntity({ name: 'keyboard', keyStates: new Set(), input: "", focus: null })    
};

/**
 * Add Selections used inside default Systems, and exported to user applications.
 * Backgrounds, Drawables, Slides, Serializables, Systems, Deletables, Targetables,
 * Togglables, Hoverables, TreeNodes, Views, Keyboards, Pointers, Menu, Options.
 *
 * @param {Context} context
 */
export let applyDefaultSelections = function(context) {
    let Backgrounds = context.createSelection("Backgrounds", e => 'backgroundColor' in e && 'depth' in e && 'bounds' in e && 'shape' in e, (a, b) => a.depth - b.depth);
    let Drawables = context.createSelection("Drawables", e => !('hidden' in e) && 'depth' in e && 'bounds' in e && ('shape' in e || 'image' in e || 'richText' in e), (a, b) => b.depth - a.depth);
    let Slides = context.createSelection("Slides", e => 'slide' in e);
    let Serializables = context.createSelection("Serializables", e => 'serializable' in e);
    let Systems = context.createSelection("Systems", e => 'runOn' in e && 'order' in e, (a, b) => a.order - b.order);
    let Deletables = context.createSelection("Deletables", e => 'toDelete' in e);
    let Targetables = context.createSelection("Targetables", e => e.targetable && 'depth' in e && 'bounds' in e, (a, b) => a.depth - b.depth);
    let Togglables = context.createSelection("Togglables", e => 'toggleStyle' in e);
    let Hoverables = context.createSelection("Hoverables", e => 'hoverGroup' in e && 'hoverStyle' in e);
    let TreeNodes = context.createSelection("TreeNodes", e => 'children' in e);
    let Views = context.createSelection("Views", e => 'origin' in e && 'bounds' in e && 'depth' in e);
    let Keyboards = context.createSelection("Keyboards", e => 'keyStates' in e && 'focus' in e);
    let Pointers = context.createSelection("Pointers", e => 'pointerId' in e);
    let Menu = context.createSelection("Menu", e => 'menu' in e);
    let Options = context.createSelection("Options", e => 'optionGroup' in e);
};

/**
 * Add base Systems to the Context.
 * PointerTargetSystem, PointerClickSystem, PointeDragSystem, ToggleSystem, HoverSystem,
 * TextEditSystem, FocusSystem, ShortcutSystem, DeleteTmpSystem, DeleteSystem.
 *
 * @param {Context} context
 */
export let applyDefaultSystems = function(context) {
    context.addSystem(PointerTargetSystem, EVENTS.POINTER_INPUT, 1);
    context.addSystem(PointerClickSystem, EVENTS.POINTER_INPUT, 21);
    context.addSystem(PointerDragSystem, EVENTS.POINTER_INPUT, 22);
    context.addSystem(ToggleSystem, EVENTS.POINTER_INPUT, 40);
    context.addSystem(HoverSystem, EVENTS.POINTER_INPUT, 41);
    context.addSystem(TextEditSystem, EVENTS.KEYBOARD_INPUT, 42);
    context.addSystem(FocusSystem, EVENTS.POINTER_INPUT, 43);
    context.addSystem(ShortcutSystem, EVENTS.KEYBOARD_INPUT, 20);
    context.addSystem(DeleteTmpSystem, EVENTS.ALL, 100);
    context.addSystem(DeleteSystem, EVENTS.ALL, 101);
}
