/**
 * Example vector painter application using Polyphony
 * Allows drawing basic shapes and manipulating them
 * 
 * Date: 21 Feb 2019
 */
import * as poly from './Polyphony.js';

let context = new poly.Context("default");
context.canvasEntity = poly.Canvas(context, 0, 0, 800, 800, { backgroundColor: [255, 255, 255]});

/**
 * Reset - deletes all Entities from the canvas
 */
let ResetSystem = function ResetSystem(context) {
    if (resetButton.tmpClickBy) {
        for (let child of context.canvasEntity.children) {
            if (child.svg)
                child.svg.remove();
            child.delete();
        }
        context.canvasEntity.children = [];
        for (let keyboard of context.selections["Keyboards"])
            keyboard.focus = null;
    }
};

/**
 * Save - serializes all Recordable Entities into SVG
 * NodeJS cannot open file dialogs, so it is saved in snapshot.svg
 */
let SaveSystem = function SaveSystem(context) {
    if (saveButton.tmpClickBy || context.selections["Keyboards"][0].tmpShortcut === "s") {
        let canvasBounds = context.canvasEntity.bounds;
        let svg = `<svg xmlns="http://www.w3.org/2000/svg" 
                    width="${canvasBounds.width}" 
                    height="${canvasBounds.height}" 
                    viewbox="${canvasBounds.x} ${canvasBounds.y} ${canvasBounds.width} ${canvasBounds.height}">`;

        for (let child of context.canvasEntity.children) {
            let bounds = child.bounds;
            if (child.shape === poly.SHAPE.RECTANGLE)
                svg += `<rect x="${bounds.x}" y="${bounds.y}" width="${bounds.width}" height="${bounds.height}"`;
            else if (child.shape === poly.SHAPE.ELLIPSE)
                svg += `<ellipse cx="${bounds.x + bounds.width / 2}" cy="${bounds.y + bounds.height / 2}" rx="${bounds.width / 2}" ry="${bounds.height / 2}"`;

            let bgColor = child.backgroundColor;
            let bdColor = child.border.color;
            if (bgColor)
                svg += ` fill="rgb(${bgColor[0]},${bgColor[1]},${bgColor[2]})"`;
            if (bdColor)
                svg += ` stroke="rgb(${bdColor[0]},${bdColor[1]},${bdColor[2]})"`;
            svg += '/>';
        }
        svg += '</svg>';

        var myBlob = new Blob([svg], { type: "text/plain" });
        const url = URL.createObjectURL(myBlob);
        download(url, 'snapshot.svg');
    }
};

let download = (path, filename) => {
    const anchor = document.createElement('a');
    anchor.href = path;
    anchor.download = filename;
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
};        

/**
 * Move - relies on poly.PointerDragSystem to (i) move shapes on the canvas, and
 *   (ii) swap shapes when dragged on side buttons
 */
let MoveTool = function MoveTool(context) {
    if (moveButton.tmpClickBy)
        delete context.getSystem("PointerDragSystem").disabled;
    if (!moveButton.toggled)
        return;
    for (let pointer of context.selections["Pointers"]) {
        let dropped = pointer.tmpDropped;
        let target = pointer.target;
        if (!dropped)
            continue;
        let dBounds = dropped.bounds;
        let cBounds = context.canvasEntity.bounds;
        if (target === resetButton) {
            context.canvasEntity.children.splice(context.canvasEntity.children.indexOf(dropped), 1);
            for (let k of context.selections["Keyboards"]) {
                if (k.focus === dropped)
                    k.focus = null;
            }
            dropped.delete();
        } else if (target === rectButton) {
            dropped.shape = poly.SHAPE.RECTANGLE;
            dropped.coordinates = new poly.Coordinates(pointer.pressPosition.x, pointer.pressPosition.y);
            dropped.bounds = dBounds.setX(pointer.pressPosition.x - dBounds.width / 2).setY(pointer.pressPosition.y - dBounds.height / 2);
        } else if (target === ovalButton) {
            dropped.shape = poly.SHAPE.ELLIPSE;
            dropped.coordinates = new poly.Coordinates(pointer.pressPosition.x, pointer.pressPosition.y);
            dropped.bounds = dBounds.setX(pointer.pressPosition.x - dBounds.width / 2).setY(pointer.pressPosition.y - dBounds.height / 2);
        } else if (target == lineButton) {
            dropped.shape = poly.SHAPE.LINE;
            dropped.coordinates = new poly.LineCoordinates(pointer.pressPosition.x - dBounds.width / 2, pointer.pressPosition.y - dBounds.height / 2, pointer.pressPosition.x + dBounds.width / 2, pointer.pressPosition.y + dBounds.height / 2)
            dropped.bounds = dBounds.setX(pointer.pressPosition.x).setY(pointer.pressPosition.y);
        } else if (dBounds.x < cBounds.x || dBounds.x + dBounds.width >= cBounds.x + cBounds.width || dBounds.y < dBounds.y || dBounds.y + dBounds.height >= cBounds.y + cBounds.height) {
            dropped.bounds = dBounds.setX(pointer.pressPosition.x - dBounds.width / 2).setY(pointer.pressPosition.y - dBounds.height / 2);
            if (dropped.shape == poly.SHAPE.LINE)
                dropped.coordinates.moveTo(pointer.pressPosition.x, pointer.pressPosition.y);
            else
                dropped.coordinates.setX(pointer.pressPosition.x).setY(pointer.pressPosition.y);
        }
    }
};

/**
 * Draw - Allows creating new shapes by press&drag, while constraining the
 *   mouse cursor inside the canvas
 */
let DrawTool = function DrawTool(context) {
    if (rectButton.tmpClickBy || ovalButton.tmpClickBy || lineButton.tmpClickBy)
        context.getSystem("PointerDragSystem").disabled = true;
    if (!rectButton.toggled && !ovalButton.toggled && !lineButton.toggled)
        return;
    for (let p of context.selections["Pointers"]) {
        let d = p.drawing;
        let pr = p.pressPosition;        
        let pos = p.coordinates;
        let cb = context.canvasEntity.bounds;
        let hasPressedInCanvas = poly.Bounds.contains(context.canvasEntity, pr);
        console.log(!d + "  " + p.buttons + " " + p.tmpMotion + " " + hasPressedInCanvas);
        if (!d && p.buttons & 1 && p.tmpMotion && hasPressedInCanvas) {
            function random(a) { return Math.floor(Math.random() * a) };   
            let type = rectButton.toggled ? poly.SHAPE.RECTANGLE : (ovalButton.toggled ? poly.SHAPE.ELLIPSE : poly.SHAPE.LINE);
            let extraComponents = {
                border: new poly.Border([100, 100, 100], 1), 
                backgroundColor: [128 + random(128), 128 + random(128), 128 + random(128)],
                richText: new poly.RichText('', null, 1, 10, 10, 4, true, { border: new poly.Border([0, 191, 255], 2) })
            };
            if (type == poly.SHAPE.LINE) extraComponents.coordinates = new poly.LineCoordinates(pr.x, pr.y, pos.x, pos.y);
            
            p.drawing = d = poly.Shape(context, type, pr.x, pr.y, extraComponents);
            d.depth = -1000;
            context.addToAllSelections(d);
            context.canvasEntity.children.push(d);
        }
        if (d) {
            if (!hasPressedInCanvas)
                pos.setX(Math.min(Math.max(pos.x, cb.x), cb.x + cb.width)).setY(Math.min(Math.max(pos.y, cb.y), cb.y + cb.height));

            if (d.shape == poly.SHAPE.LINE) {
                d.coordinates.setStart(pr.x, pr.y).setEnd(pos.x, pos.y);
                d.bounds.setW(Math.abs(pr.x - pos.x)).setH(Math.abs(pr.y - pos.y));
            }
            else if (d.shape == poly.SHAPE.ELLIPSE || d.shape == poly.SHAPE.RECTANGLE) {
                d.coordinates.moveTo(pr.x, pr.y);
                d.bounds.setW(Math.abs(pr.x - pos.x)).setH(Math.abs(pr.y - pos.y));                
            }

            if (p.tmpReleased === 0)
                delete p.drawing;

            console.log(d.coordinates.x + " " + d.coordinates.y);
        }
    }
};

/**
 * Rotate - Rotates existing shapes
 *
let RotateTool = poly.createEntity(function RotateTool() {
    if (rotaButton.tmpClickBy)
        poly.PointerDragSystem.disabled = true
    if (!rotaButton.toggled)
        return
    for (let p of poly.Pointers) {
        let d = p.tmpDropped
        let t = p.target
        if save(!d)
            continue
        if (t === resetButton)
            d.angle = 0
        else {
            continue
        }
    }
}, { runOn: poly.EVENTS.POINTER_INPUT, order: 64 })


**/

let TestSystem = function TestSystem(context) {
    if (context.selections["Keyboards"][0].tmpShortcut === "e")
        for (let e in TreeNodes) console.log(e);
};

/**
 *  se base sur FocusSystem pour déterminer l'entité à modifier
 */
let MenuTool = function MenuTool(context) {
    for (let m of context.selections["Menu"]) {
        if (m.tmpClickBy) {
            context.getSystem("PointerDragSystem").disabled = true;
        }
        if (!m.toggled)
            continue
        for (let p of context.selections["Pointers"]) {
            let d = p.drawing;
            let o = p.option;
            let pr = p.pressPosition;
            let pos = p.coordinates;
            let leftClick = p.buttons & 1;
            if (!d && leftClick && poly.Bounds.contains(context.canvasEntity, pr)) {
                p.drawing = d = m.menu.init(pr);
                d.draws = d.draw();
                context.canvasEntity.children.push(d.draws);
                d.options = new Array();
                for (let op of context.selections["Options"]) {
                    if (m.menu.controls.optionGroup === op.optionGroup)
                        d.options.push(op);
                }
                d.focus = context.selections["Keyboards"][0].focus;
                d.width = d.focus.bounds.width;
                d.height = d.focus.bounds.height;
            }
            if (d) {
                if (p.tmpReleased === 0) {
                    delete p.drawing;
                    d.draws.toDelete = true;
                    context.updateAllSelections(d.draws);
                    context.canvasEntity.children.splice(context.canvasEntity.children.indexOf(d.draws), 1);
                    if (pr.distance(pos) > 10)
                        continue;
                    else {
                        d.focus.bounds.setW(d.width);
                        d.focus.bounds.setH(d.height);
                    }
                }
                else
                    d.behaviour(p, d);
            }
        }
    }
};

/*
 * Insert text on double click on focus element
 */
let InsertTextSystem = function InsertTextSystem(context) {
    for (let pointer of context.selections["Pointers"]) {
        if (pointer.tmpDB) {
            for (let keyboard of context.selections["Keyboards"]) {
                if (keyboard.focus && poly.Bounds.contains(keyboard.focus, pointer.coordinates)) {
                    keyboard.targetTextfield = keyboard.focus;
                }
            }
        }        
    }

    for (let keyboard of context.selections["Keyboards"]) {
        if (keyboard.focus != keyboard.targetTextfield)
            delete keyboard.targetTextfield;
    }
};

/*
 * Delete selected shape
 */
let DeleteTool = function DeleteTool(context) {
    if (deleteButton.tmpClickBy) {
        for (let keyboard of context.selections["Keyboards"]) {
            if (keyboard.focus) {
                keyboard.focus.toDelete = true;
                context.updateAllSelections(keyboard.focus);
                keyboard.focus = null;
            }
        }
    }
};

/**
 * Interface
 */
let resetButton, saveButton, moveButton, rectButton, ovalButton, lineButton, radialButton, widthRadial, heightRadial, deleteButton;
let y = 16;

resetButton = poly.Button(context, new poly.Image('../icons/reset.png', 24, 24), 16, y);
saveButton = poly.Button(context, new poly.Image('../icons/save.png', 24, 24), 16, y += 34);
moveButton = poly.RadioButton(context, new poly.Image('../icons/move.png', 24, 24), 16, y += 34, 1, { backgroundColor: [0, 191, 255] });
rectButton = poly.RadioButton(context, new poly.Image('../icons/rect.png', 24, 24), 16, y += 34, 1, { backgroundColor: [0, 191, 255] });
ovalButton = poly.RadioButton(context, new poly.Image('../icons/oval.png', 24, 24), 16, y += 34, 1, { backgroundColor: [0, 191, 255] });
lineButton = poly.RadioButton(context, new poly.Image('../icons/line.png', 24, 24), 16, y += 34, 1, { backgroundColor: [0, 191, 255] });
radialButton = poly.RadioButton(context, new poly.Image('../icons/pieScale.png', 24, 24), 16, y += 34, 1, { backgroundColor: [0, 191, 255] },
    {
        menu: {
            type: "pie",
            controls: { optionGroup: 1 },
            maxSize: 6,
            radius: 30,
            init: (pr) => {
                let r = 30;
                let origin = new poly.Coordinates(pr.x, pr.y, pr.z)
                let lastPos = new poly.Coordinates(pr.x, pr.y, pr.z)
                return {
                    draw: () => {
                        let s = poly.Shape(context, poly.SHAPE.ELLIPSE, r * 2, r * 2, { border: new poly.Border([100, 100, 100], 1), backgroundColor: [100, 100, 100], depth: -1000 } );
                        s.coordinates.moveTo(pr.x, pr.y);
                        context.addToAllSelections(s);
                        return s
                    },
                    behaviour: (p, d) => {
                        let pos = p.coordinates
                        let o = p.option
                        if (origin.distance(pos) > 30 && !o) {
                            lastPos = new poly.Coordinates(pos.x, pos.y, pos.z)
                            p.option = o = d.options[Math.floor(pos.angleBoundsDegrees(d.draws.coordinates) / (360 / d.options.length))]
                        }
                        if (o) {
                            o.method(d.focus, lastPos, pos)
                            lastPos = new poly.Coordinates(pos.x, pos.y, pos.z)
                        }
                        if (o && pr.distance(pos) <= 30)
                            p.option = o = undefined
                    }
                }
            }
        }
});
deleteButton = poly.Button(context, new poly.Image('../icons/delete.png', 24, 24), 16, y += 34);

widthRadial = context.createEntity({
    optionGroup: 1, method: (e, s, t) => {
        e.bounds.setW(e.bounds.width + t.x - s.x);
        if (e.shape == poly.SHAPE.LINE)
            e.coordinates.growW(t.x - s.x);
    }
});
heightRadial = context.createEntity({
    optionGroup: 1, method: (e, s, t) => {
        e.bounds.setH(e.bounds.height + t.y - s.y);
        if (e.shape == poly.SHAPE.LINE)
            e.coordinates.growH(t.y - s.y);
    }
});

let startPoint = poly.Shape(context, poly.SHAPE.ELLIPSE, 20, 20, { name: "start", coordinates: new poly.Coordinates(50, 50, 0), backgroundColor: [100, 100, 100] });
let endPoint = poly.Shape(context, poly.SHAPE.RECTANGLE, 20, 30, { name: "end", coordinates: new poly.Coordinates(200, 200, 0), backgroundColor: [100, 100, 100] }); 
let line = poly.LinkLine(context, startPoint, endPoint);
line.backgroundColor = [200, 200, 200];
line.border =  new poly.Border([100, 100, 100], 2);

window.onload = function () {
    let canvas_candidate = document.getElementById("canvas");

    poly.addViewPointerKeyboardToContext(context);
    poly.applyDefaultSelections(context);
    poly.applyDefaultSystems(context);

    context.addSystem(InsertTextSystem, poly.EVENTS.POINTER_INPUT, 39);
    context.addSystem(DeleteTool, poly.EVENTS.POINTER_INPUT, 59);
    context.addSystem(ResetSystem, poly.EVENTS.POINTER_INPUT, 60);
    context.addSystem(SaveSystem, poly.EVENTS.POINTER_INPUT, 61);
    context.addSystem(MoveTool, poly.EVENTS.POINTER_INPUT, 62);
    context.addSystem(DrawTool, poly.EVENTS.POINTER_INPUT, 63);
    context.addSystem(MenuTool, poly.EVENTS.POINTER_INPUT, 65);
    context.addSystem(TestSystem, poly.EVENTS.KEYBOARD_INPUT, 99);

    context.setCanvas(canvas_candidate);
    context.startRendering();

    poly.startPolyphony();
}