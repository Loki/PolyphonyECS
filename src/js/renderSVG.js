/**
 * Rendering in SVG for PolyphonyECS
 * @module renderSVG
 */

import * as poly from './Polyphony.js';

/**
 * Setup canvas for SVG rendering
 *
 * @param {*} canvas
 * @param {*} canvasWidth
 * @param {*} canvasHeight
 */
let initCanvas = function (canvas, canvasWidth, canvasHeight) {
    var svgns = "http://www.w3.org/2000/svg";
    var svgCanvas = canvas;
    
    svgCanvas.setAttributeNS(null, 'id', "canvas");
    svgCanvas.setAttributeNS(null, 'width', canvasWidth);
    svgCanvas.setAttributeNS(null, 'height', canvasHeight);
    svgCanvas.setAttributeNS(null, 'version', "1.1");

    var shapes = document.createElementNS(svgns, 'g');
    shapes.setAttributeNS(null, "id", "shapes");

    var image = document.createElementNS(svgns, 'g');
    image.setAttributeNS(null, "id", "img"); 

    svgCanvas.appendChild(shapes);
    svgCanvas.appendChild(image);
}

/**
 * Render rectangle.
 *
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} borderColor
 * @param {*} fillColor
 * @returns {*}
 */
function renderRect(x, y, width, height, borderColor, fillColor) {
    let bodySVG = document.getElementById("shapes");
    let svgns = "http://www.w3.org/2000/svg";
    let rect = document.createElementNS(svgns, 'rect');

    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    rect.setAttributeNS(null, 'x', x);
    rect.setAttributeNS(null, 'y', y);
    rect.setAttributeNS(null, 'width', width);
    rect.setAttributeNS(null, 'height', height);
    rect.setAttributeNS(null, 'stroke', borderColor);
    rect.setAttributeNS(null, 'fill', fillColor);

    return bodySVG.appendChild(rect);
};

/**
 * Render Ellipse.
 * 
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} borderColor
 * @param {*} fillColor
 * @returns {*}
 */
function renderEllipse(x, y, width, height, borderColor, fillColor) {
    let bodySVG = document.getElementById("shapes");
    let svgns = "http://www.w3.org/2000/svg";
    let ellipse = document.createElementNS(svgns, 'ellipse');

    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    ellipse.setAttributeNS(null, 'cx', x + width / 2);
    ellipse.setAttributeNS(null, 'cy', y + height / 2);
    ellipse.setAttributeNS(null, 'rx', width);
    ellipse.setAttributeNS(null, 'ry', height);
    ellipse.setAttributeNS(null, 'stroke', borderColor);
    ellipse.setAttributeNS(null, 'fill', fillColor);

    return bodySVG.appendChild(ellipse);
};

/**
 * Render Line.
 *
 * @param {*} x1
 * @param {*} y1
 * @param {*} x2
 * @param {*} y2
 * @param {*} borderColor
 * @param {*} fillColor
 * @returns {*}
 */
function renderLine(x1, y1, x2, y2, borderColor, fillColor) {
    let bodySVG = document.getElementById("shapes");
    let svgns = "http://www.w3.org/2000/svg";
    let line = document.createElementNS(svgns, 'line');

    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";
    borderColor = fillColor;

    line.setAttributeNS(null, 'x1', x1);
    line.setAttributeNS(null, 'y1', y1);
    line.setAttributeNS(null, 'x2', x2);
    line.setAttributeNS(null, 'y2', y2);
    line.setAttributeNS(null, 'stroke', borderColor);
    line.setAttributeNS(null, 'fill', fillColor);
    line.setAttributeNS(null, 'width', "5px");

    return bodySVG.appendChild(line);
};

/**
 * Render Image.
 *
 * @param {*} src
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} opacity
 * @returns {*}
 */
function renderImg(src, x, y, width, height, opacity) {
    let bodySVG = document.getElementById("img");
    let svgns = "http://www.w3.org/2000/svg";
    let img = document.createElementNS(svgns, 'image');

    img.setAttributeNS(null, 'href', src);
    img.setAttributeNS(null, 'x', x);
    img.setAttributeNS(null, 'y', y);
    img.setAttributeNS(null, 'width', width);
    img.setAttributeNS(null, 'height', height);

    return bodySVG.appendChild(img);
};

/**
 * Render Text.
 *
 * @param {*} text
 * @param {*} x
 * @param {*} y
 * @param {*} height
 * @param {*} width
 * @param {*} fillColor
 * @returns {*}
 */
function renderText(text, x, y, height, width, fillColor) {
    let bodySVG = document.getElementById("img");
    let svgns = "http://www.w3.org/2000/svg";
    let textEl = document.createElementNS(svgns, 'text');

    if (!fillColor) fillColor = "transparent";

    textEl.setAttributeNS(null, 'x', x - width / 2);
    textEl.setAttributeNS(null, 'y', y);
    textEl.setAttributeNS(null, 'width', width);
    textEl.setAttributeNS(null, 'height', height);
    textEl.innerHTML = text;

    return bodySVG.appendChild(textEl);
};

/**
 * Render PolyLine.
 *
 * @param {*} x
 * @param {*} y
 * @param {*} points
 * @param {*} borderColor
 * @param {*} fillColor
 * @returns {*}
 */
function renderPolyLine(x, y, points, borderColor, fillColor) {
    let bodySVG = document.getElementById("shapes");
    let svgns = "http://www.w3.org/2000/svg";
    let polyLine = document.createElementNS(svgns, 'path');
    
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    let path = "M " + (points[0][0] + x) + " " + (points[0][1] + y) + " ";
    for(let i = 1; i < points.length; i++){
        path += "L " + (points[i][0] + x) + " " + (points[i][1] + y) + " ";
    }

    polyLine.setAttributeNS(null, 'd', path);
    polyLine.setAttributeNS(null, 'stroke', borderColor);
    polyLine.setAttributeNS(null, 'fill', fillColor);
    polyLine.setAttributeNS(null, 'width', "5px");

    return bodySVG.appendChild(polyLine);
};


/**
 * Update values of existing Rectangle.
 *
 * @param {*} obj
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} borderColor
 * @param {*} fillColor
 * @returns {*}
 */
function updateRect(obj, x, y, width, height, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    obj.setAttributeNS(null, 'x', x);
    obj.setAttributeNS(null, 'y', y);
    obj.setAttributeNS(null, 'width', width);
    obj.setAttributeNS(null, 'height', height);
    obj.setAttributeNS(null, 'stroke', borderColor);
    obj.setAttributeNS(null, 'fill', fillColor);

    return obj;
};

/**
 * Update values of existing Ellipse.
 *
 * @param {*} obj
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} borderColor
 * @param {*} fillColor
 * @returns {*}
 */
function updateEllipse(obj, x, y, width, height, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";

    obj.setAttributeNS(null, 'cx', x + width / 2);
    obj.setAttributeNS(null, 'cy', y + height / 2);
    obj.setAttributeNS(null, 'rx', width / 2);
    obj.setAttributeNS(null, 'ry', height / 2);
    obj.setAttributeNS(null, 'stroke', borderColor);
    obj.setAttributeNS(null, 'fill', fillColor);

    return obj;
};

/**
 * Update values of existing Line.
 *
 * @param {*} obj
 * @param {*} x1
 * @param {*} y1
 * @param {*} x2
 * @param {*} y2
 * @param {*} borderColor
 * @param {*} fillColor
 * @returns {*}
 */
function updateLine(obj, x1, y1, x2, y2, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";
    borderColor = fillColor;

    obj.setAttributeNS(null, 'x1', x1);
    obj.setAttributeNS(null, 'y1', y1);
    obj.setAttributeNS(null, 'x2', x2);
    obj.setAttributeNS(null, 'y2', y2);
    obj.setAttributeNS(null, 'stroke', borderColor);
    obj.setAttributeNS(null, 'fill', fillColor);
    obj.setAttributeNS(null, 'width', "5px");

    return obj;
};

/**
 * Update values of existing Polyline.
 *
 * @param {*} obj
 * @param {*} x
 * @param {*} y
 * @param {*} points
 * @param {*} borderColor
 * @param {*} fillColor
 */
function updatePolyLine(obj, x, y, points, borderColor, fillColor) {
    if (!borderColor) borderColor = "transparent";
    if (!fillColor) fillColor = "transparent";
    borderColor = fillColor;

    let path = "M " + (points[0][0] + x) + " " + (points[0][1] + y) + " ";
    for(let i = 1; i < points.length; i++){
        path += "L " + (points[i][0] + x) + " " + (points[i][1] + y) + " ";
    }

    obj.setAttributeNS(null, 'd', path);
    obj.setAttributeNS(null, 'stroke', borderColor);
    obj.setAttributeNS(null, 'fill', fillColor);
    obj.setAttributeNS(null, 'width', "5px");
};

/**
 * Update values of existing Image.
 *
 * @param {*} obj
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @param {*} opacity
 * @returns {*}
 */
function updateImg(obj, x, y, width, height, opacity) {
    obj.setAttributeNS(null, 'x', x);
    obj.setAttributeNS(null, 'y', y);
    obj.setAttributeNS(null, 'width', width);
    obj.setAttributeNS(null, 'height', height);

    return obj;
};

/**
 * Update values of existing Text.
 *
 * @param {*} obj
 * @param {*} text
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 * @returns {*}
 */
function updateText(obj, text, x, y, width, height) {
    obj.innerHTML = text;
    obj.setAttributeNS(null, 'x', x - width / 2);
    obj.setAttributeNS(null, 'y', y);
    obj.setAttributeNS(null, 'width', width);
    obj.setAttributeNS(null, 'height', height);

    return obj;
};

/**
 * Check if SVG tag matches PolyphonyECS.SHAPE.
 *
 * @param {Entity} e
 * @returns {boolean}
 */
function checkSvgMatchType(e) {
    switch (e.shape) {
        case poly.SHAPE.RECTANGLE:
            return e.svg.tagName == "rect";
        case poly.SHAPE.ELLIPSE:
            return e.svg.tagName == "ellipse";
        case poly.SHAPE.LINE:
            return e.svg.tagName == "line";
        case poly.SHAPE.POLYLINE:
            return e.svg.tagName == "path";
        default:
            throw new Error("Unsupported SHAPE");
            break;
    }
}

/**
 * Set all the listeners to redirect JS Events to Polyphony.
 */
let setEventListeners = function () {
    document.addEventListener("mouseup", function (event) { poly.eventStack.push({ type: "mouseup", event: event }) });
    document.addEventListener("mousedown", function (event) { poly.eventStack.push({ type: "mousedown", event: event }) });
    document.addEventListener("mousemove", function (event) { poly.eventStack.push({ type: "mousemove", event: event }) });
    document.addEventListener("dblclick", function (event) { poly.eventStack.push({ type: "mousedb", event: event }) });
    document.addEventListener("keydown", function (event) { poly.eventStack.push({ type: "keydown", event: event }) });
    document.addEventListener("keyup", function (event) { poly.eventStack.push({ type: "keyup", event: event }) });
    document.addEventListener("wheel", function (event) { poly.eventStack.push({ type: "wheel", event: event }) });
}

/**
 * Create SVG tag from Entity.
 *
 * @param {Entity} entity
 * @param {*} shape
 * @param {*} image
 * @param {*} borderColor
 * @param {*} backgroundColor
 */
let createSVG = function (entity, shape, image, borderColor, backgroundColor) {
    if (image)
        image.svg = renderImg(image.src, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height);

    if (shape === poly.SHAPE.RECTANGLE)
        entity.svg = renderRect(entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height, borderColor, backgroundColor);
    else if (shape === poly.SHAPE.ELLIPSE) {
        entity.svg = renderEllipse(entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height, borderColor, backgroundColor);
    }
    else if (shape === poly.SHAPE.LINE) {
        entity.svg = renderLine(entity.coordinates.x1, entity.coordinates.y1, entity.coordinates.x2, entity.coordinates.y2, borderColor, backgroundColor);
    }
    else if (shape === poly.SHAPE.POLYLINE){
        entity.svg = renderPolyLine(entity.coordinates.x, entity.coordinates.y, entity.points, borderColor, backgroundColor);
    }

    if (entity.richText)
        entity.richText.svg = renderText(entity.richText.str, entity.coordinates.x, entity.coordinates.y, entity.bounds.height, entity.bounds.width, "rgb(0,0,0)");
}

/**
 * Update existing SVG from Entity.
 *
 * @param {Entity} entity
 * @param {*} shape
 * @param {*} image
 * @param {*} borderColor
 * @param {*} backgroundColor
 */
let updateSVG = function (entity, shape, image, borderColor, backgroundColor) {
    if (!checkSvgMatchType(entity)) {
        entity.svg.remove();
        delete entity.svg;
        return;
    }

    if (image)
        updateImg(image.svg, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height);

    if (shape === poly.SHAPE.RECTANGLE)
        updateRect(entity.svg, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height, borderColor, backgroundColor);
    else if (shape === poly.SHAPE.ELLIPSE) {
        updateEllipse(entity.svg, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height, borderColor, backgroundColor);
    }
    else if (shape === poly.SHAPE.LINE) {
        updateLine(entity.svg, entity.coordinates.x1, entity.coordinates.y1, entity.coordinates.x2, entity.coordinates.y2, borderColor, backgroundColor);
    }
    else if (shape === poly.SHAPE.POLYLINE){
        updatePolyLine(entity.svg, entity.coordinates.x, entity.coordinates.y, entity.points, borderColor, backgroundColor);
    }
    else if (!shape && entity.svg)
        entity.svg.remove();

    if (entity.richText) {
        updateText(entity.richText.svg, entity.richText.str, entity.coordinates.x, entity.coordinates.y, entity.bounds.width, entity.bounds.height);
    }
}

/**
 * Delete SVG from Entity
 *
 * @param {Entity} entity
 */
let deleteSVG = function (entity) {
    if (entity.svg)
        entity.svg.remove();
    if (entity.richText && entity.richText.svg)
        entity.richText.svg.remove();
}

/**
 * Generate color string from array
 *
 * @param {*} array
 * @returns {string}
 */
let colorArrayToString = function (array) {
    if (!array)
        return null

    if (array.length == 3)
        return "rgb(" + array[0] + "," + array[1] + "," + array[2] + ")";
    if (array.length == 4)
        return "rgba(" + array[0] + "," + array[1] + "," + array[2] + "," + array[3] + ")";
    return null
}

/**
 * Setup rendering environment.
 */
let initRender = function () {
    setEventListeners();
}

/**
 * Software renderer - draws filled shapes, borders, and icons of Entities
 * in painting order.
 *
 * @param {poly.Context} context
 */
let SoftRenderSystem = function SoftRenderSystem(context) {        
    for (let entity of context.selections["Drawables"]) {

        if (entity.toDelete) {
            deleteSVG(entity);
            continue;
        }

        // filled shapes
        let backgroundColor = colorArrayToString(entity.backgroundColor);
        let borderColor = (entity.border) ? colorArrayToString(entity.border.color) : null;

        if (entity.svg && (entity.bounds.has_changed || entity.coordinates.has_changed || entity.has_changed)) {
            updateSVG(entity, entity.shape, entity.image, borderColor, backgroundColor);                
            delete entity.bounds.has_changed
        }
            
        if (!entity.svg) {
            createSVG(entity, entity.shape, entity.image, borderColor, backgroundColor);
        }
    }
};


/**
 * Propagate change on linked entities - Look for entities changed, to flag 
 * dependent entities as changed too.
 *
 * @param {poly.Context} context
 */
let DependencyChangePropagation = function DependencyChangePropagation(context){
    let entitiesToVisit = [];
    let visitedEntities = new Set();
    let currentEntity;
    for(let entity of context.entities){
        if(entity.dependency && entityHasChanged(entity))
            entitiesToVisit.push(entity);
    }

    while(entitiesToVisit.length > 0){
        currentEntity = entitiesToVisit.shift();
        if(currentEntity.dependency && entityHasChanged(currentEntity)){
            for(let d of currentEntity.dependency){
                d.has_changed = true;
                if(!visitedEntities.has(d)) entitiesToVisit.push(d);
            }
            visitedEntities.add(currentEntity);
        }
    }
}

let entityHasChanged = function(entity){
    return entity.has_changed || entity.coordinates?.has_changed || entity.bounds?.has_changed;
}
    
export { initRender, initCanvas, colorArrayToString, SoftRenderSystem, DependencyChangePropagation }