import { EditorView, basicSetup } from "codemirror"
import { EditorState, StateEffect, StateField, Facet, RangeSetBuilder } from "@codemirror/state"
import { javascript } from "@codemirror/lang-javascript"
import { keymap, Decoration, ViewPlugin } from "@codemirror/view"
import { walkthroughFiles } from "./walkthroughExamples.js"


let content = `import * as Polyphony from '../js/Polyphony.js';
                
let canvas_candidate = document.getElementById("canvas");
let canvas = Polyphony.Canvas(32, 2, 800, 800);
context.canvasEntity = canvas;
Polyphony.setRendering(Polyphony.RENDER_CANVAS);
Polyphony.startRendering(canvas_candidate, canvas);

view.bounds.setW(canvas.width).setH(canvas.height);

`;

let fileIndex = 0;
let readOnlyLineRanges;

const baseTheme = EditorView.baseTheme({
    "&light .readOnlyLine": { backgroundColor: "#bbbbbb" },
    "&dark .readOnlyLine": { backgroundColor: "#bbbbbb" }
});

const showReadOnly = ViewPlugin.fromClass(class {
    constructor(view) {
        this.decorations = readOnlyDeco(view);
    }

    update(update) {
        if (update.docChanged || update.viewportChanged)
            this.decorations = readOnlyDeco(update.view);
    }
},
    {
        decorations: v => v.decorations
    });

function readOnlyBackground(options = {}) {
    return [
        baseTheme,
        showReadOnly
    ]
}

const readOnlyEffect = Decoration.line({
    attributes: { class: "readOnlyLine" }
});

function readOnlyDeco(view) {
    let builder = new RangeSetBuilder();
    let readOnlyRanges = getReadOnlyLineRanges(view);
    for (let { from, to } of view.visibleRanges) {
        for (let pos = from; pos <= to;) {
            let line = view.state.doc.lineAt(pos);
            for (let i = 0; i < readOnlyRanges.length; i++) {
                if (line.number >= readOnlyRanges[i].from && line.number <= readOnlyRanges[i].to) {
                    builder.add(line.from, line.from, readOnlyEffect);
                }
            }            
            pos = line.to + 1;
        }                       
    }
    return builder.finish();
}

const getReadOnlyLineRanges = function (view) {
    let walkthroughFile = walkthroughFiles[fileIndex];
    let readOnlyRanges = [];

    walkthroughFile.readOnlyRanges.forEach(function (range) {
        let startRange = (range.from >= 0) ? range.from : view.state.doc.lines + range.from + 1;
        let endRange = (range.to >= 0) ? range.to : view.state.doc.lines + range.to + 1;

        readOnlyRanges.push({ from: startRange, to: endRange });
    });

    return readOnlyRanges;
}

const getReadOnlyRanges = targetState => {
    let walkthroughFile = walkthroughFiles[fileIndex];
    let readOnlyRanges = [];
    
    walkthroughFile.readOnlyRanges.forEach(function (range) {
        let startRange = (range.from >= 0) ? range.from : targetState.doc.lines + range.from + 1;
        let endRange = (range.to >= 0) ? range.to : targetState.doc.lines + range.to + 1;

        readOnlyRanges.push({ from: targetState.doc.line(startRange).from, to: targetState.doc.line(endRange).to });
    });
    
    return readOnlyRanges;
}

content = walkthroughFiles[fileIndex].content;


// From https://andrebnassis.github.io/codemirror-readonly-ranges/ adapted in native JS
export const preventModifyTargetRanges = getReadOnlyRanges =>
    EditorState.changeFilter.of(tr => {
        try {
            const readOnlyRangesBeforeTransaction = getReadOnlyRanges(tr.startState)
            const readOnlyRangesAfterTransaction = getReadOnlyRanges(tr.state)

            for (let i = 0; i < readOnlyRangesBeforeTransaction.length; i++) {
                const targetFromBeforeTransaction = readOnlyRangesBeforeTransaction[i].from ?? 0;
                const targetToBeforeTransaction = readOnlyRangesBeforeTransaction[i].to ?? tr.startState.doc.line(tr.startState.doc.lines).to;

                const targetFromAfterTransaction = readOnlyRangesAfterTransaction[i].from ?? 0;
                const targetToAfterTransaction = readOnlyRangesAfterTransaction[i].to ?? tr.state.doc.line(tr.state.doc.lines).to;

                if (tr.startState.sliceDoc(targetFromBeforeTransaction, targetToBeforeTransaction) !== tr.state.sliceDoc(targetFromAfterTransaction, targetToAfterTransaction))
                    return false;                
            }
        } catch (e) {
            return false
        }
        return true
    });


let view = new EditorView({
    doc: content,
    extensions: [basicSetup, javascript(), preventModifyTargetRanges(getReadOnlyRanges), readOnlyBackground()],
    parent: document.body.getElementsByClassName("code")[0]
});

delete keymap['default']['Cmd-L'];
delete keymap['default']['Cmd-T'];
delete keymap['default']['Cmd-W'];
delete keymap['default']['Cmd-J'];
delete keymap['default']['Cmd-R'];

function evaluateSrcDoc() {
    var code = view.state.doc.toString();

    const webpage = `<!DOCTYPE html>
        <html>
            <head>
                <title>Evalbox's Frame</title>
             
            </head>
            <body>
            </body>
            <script>
                var canvas = document.createElement("canvas");
                canvas.setAttribute("id", "canvas");
                canvas.width = window.innerWidth;
                canvas.height = window.innerHeight
                document.body.appendChild(canvas);
            </script>
            <script type="module">
                import * as Polyphony from '../js/Polyphony.js';

                let context = new Polyphony.Context("default");
                let view = context.createEntity({ name: 'view', bounds: new Polyphony.Bounds(1024, 768), depth: 1, origin: new Polyphony.Coordinates(), coordinates: new Polyphony.Coordinates(0, 0) });
                let pointer = context.createEntity({ name: 'mouse', coordinates: new Polyphony.Coordinates(), pointerId: 0, buttons: 0 })
                let keyboard = context.createEntity({ name: 'keyboard', keyStates: new Set(), input: "", focus: null })    

                Polyphony.applyDefaultSelections(context);
                Polyphony.applyDefaultSystems(context);

                let canvas_candidate = document.getElementById("canvas");
                let canvas = Polyphony.Canvas(context, 32, 2, 800, 800);
                context.canvasEntity = canvas;
                context.setCanvas(canvas_candidate);
                context.startRendering();

                view.bounds.setW(canvas.width).setH(canvas.height);
                Polyphony.startPolyphony();
                `
                + code +`
            </script>   
        </html>`;

    var frame = document.getElementById("SandBox");

    frame.srcdoc = webpage;
}

function reset() {
    var frame = document.getElementById('SandBox');
    frame.srcdoc = "";
}

function nextExercise() {
    if (fileIndex == walkthroughFiles.length)
        throw new RangeError("File index cannot be more than 1");

    fileIndex += 1;
    reset();
    content = walkthroughFiles[fileIndex].content;

    view.destroy();
    view = new EditorView({
        doc: content,
        extensions: [basicSetup, javascript(), preventModifyTargetRanges(getReadOnlyRanges), readOnlyBackground()],
        parent: document.body.getElementsByClassName("code")[0]
    });
}

function previousExercise() {
    if (fileIndex == 0)
        throw new RangeError("File index cannot be under 0");

    fileIndex -= 1;
    reset();
    content = walkthroughFiles[fileIndex].content;

    view.destroy();
    view = new EditorView({
        doc: content,
        extensions: [basicSetup, javascript(), preventModifyTargetRanges(getReadOnlyRanges), readOnlyBackground()],
        parent: document.body.getElementsByClassName("code")[0]
    });
}

document.getElementById("Reset").addEventListener('click', reset);
document.getElementById('RunCode').addEventListener('click', evaluateSrcDoc);
document.getElementById('Next').addEventListener('click', nextExercise);
document.getElementById('Previous').addEventListener('click', previousExercise);