import * as poly from '../src/js/Polyphony.js'

//let canvas = poly.Canvas(36, 2, 602, 476, { name: 'canvas' })

//let p = poly.createEntity({ name: 'mockMouse', coordinates: new poly.Coordinates(), buttons: 0})

function pressLeftClick(pointer) { pointer.buttons = 1; pointer.tmpPressed = 0}
function releaseLeftClick(pointer) { pointer.buttons = 1; pointer.tmpPressed = null; pointer.tmpReleased = 0 }
function TestSystem(context) {
    for(const entity of context.entities){
        if(entity?.name.includes("test"))
            entity.modified = true;
    }
};
function TestSystemAlt(context) {
    for(const entity of context.entities){
        if(entity?.name.includes("alt"))
            entity.modified = true;
    }
};
function testRadio(context, name, group) {
    let entity = poly.RadioButton(context, name, 0, 0, group, { backgroundColor: [100, 100, 100]});
    entity.name = name;
    return entity;
}
function testCheckBox(context, name, group) {
    let entity = poly.CheckBox(context, name, 0, 0, group, { backgroundColor: [100, 100, 100]});
    entity.name = name;
    return entity;
}
//function clickAt(p, x, y, z = 0) { p.coordinates = new poly.Coordinates(x, y, z); poly.MetaSystem(poly.EVENTS.POINTER_INPUT); click(p) }
//function moveBy(p, x, y, z = 0) { c = p.coordinates; p.coordinates = new poly.Coordinates(c.x + x, c.y + y, c.z + z), poly.MetaSystem(poly.EVENTS.POINTER_INPUT) }
//function moveTo(p, x, y, z = 0) { p.coordinates = new poly.Coordinates(x, y, z); poly.MetaSystem(poly.EVENTS.POINTER_INPUT) }

describe("Test Context", () => {
    test("Create a new context", () => {
        // No Setup
        // Exercising
        let newContext = new poly.Context("default");
        // Expectation
        expect(newContext.name).toBe("default");
    });

    test("New context listed in Polyphony", () => {
        // No Setup
        // Exercising
        let newContext = new poly.Context("default");
        // Expectation
        expect(poly.getContext("default")).toBe(newContext);
    });

    test("Clear selected context", () => {
        // Setup
        let contextTest = new poly.Context("default");
        let entityTest = contextTest.createEntity({ name: "testEntity" });
        // Exercising
        contextTest.clearContext();
        // Expectation
        expect(poly.getContext("default").entities).toEqual(new Set());
        expect(poly.getContext("default").updated).toEqual(new Set());
        expect(poly.getContext("default").deleted).toEqual(new Set());
        expect(poly.getContext("default").selections.length).toEqual(0);
    });

    afterEach(() => {
        poly.getContext("default").clearContext();
    });
});
   
describe("Test Entities", () => {
    test("Create entity", () => {
        // No Setup
        let context = new poly.Context("default");
        // Exercising
        let t = context.createEntity({ name: "test" });

        // Expectation
        expect(t.name).toBe("test");
    });

    test("Delete component", () => {
        // Setup
        let context = new poly.Context("default");
        let t = context.createEntity({ name: "test", color: "blue" });

        // Exercising
        delete (t.color);

        // Expectation
        expect(t.color).toBe(undefined);
        expect(t.name).toBe("test");
    });
 
    test("Add component", () => {
        // Setup
        let context = new poly.Context("default");
        let t = context.createEntity({ name: "test" });

        // Exercising
        t.size = 12;

        // Expectation
        expect(t.size).toBe(12);
    });

    test("Switch components from one profile to the other", () => {
        // Setup
        let context = new poly.Context("default");
        let entity = context.createEntity({ backgroundColor: "red", size: 20, altProfile: { backgroundColor: "blue", width: 10 } });

        // Exercise
        poly.ReplaceComponentsInEntity(entity, entity.altProfile, entity._altProfile);

        // Expectations
        expect(entity.backgroundColor).toBe("blue");
        expect(entity.size).toBe(20);
        expect(entity.width).toBe(10);
    });

    test("Delete undefined components", () => {
        // Setup
        let context = new poly.Context("default");
        let entity = context.createEntity( { test: "a", cmpntToDelete: { test: null }});

        // Exercise
        poly.DeleteUndefinedComponents(entity, entity.cmpntToDelete);
        
        // Expectations
        expect(entity.backgroundColor).toBeUndefined();
    });

    afterEach(() => {
        poly.getContext("default").clearContext();
    });
});

describe("Test Complex Blueprint Entities", () =>{
    test("Test LinkLines made of two points", () => {
        // Setup
        let context = new poly.Context("default");
        let startPoint = context.createEntity({coordinates: new poly.Coordinates(4, 11, 0)});
        let endPoint = context.createEntity({coordinates: new poly.Coordinates(12, 13, 0)});
        
        // Exercise
        let linkLine = poly.LinkLine(context, startPoint, endPoint);

        // Expectations
        expect(linkLine.coordinates.x).toBe(4);
        expect(linkLine.coordinates.y).toBe(11);
    });

    test("Test LinkLines with one point moved", () => {
        // Setup
        let context = new poly.Context("default");
        let startPoint = context.createEntity({coordinates: new poly.Coordinates(4, 11, 0)});
        let endPoint = context.createEntity({coordinates: new poly.Coordinates(12, 13, 0)});
        let linkLine = poly.LinkLine(context, startPoint, endPoint);
        
        // Exercise
        endPoint.coordinates = new poly.Coordinates(16, 17, 0);

        // Expectations
        expect(linkLine.coordinates.x).toBe(4);
        expect(linkLine.coordinates.y).toBe(11);
    });
});

describe("Test Selections", () => {
    test("Add to one selection", () => {        
        // Setup
        let context = new poly.Context("default");
        let TestSelection = context.createSelection("TestSelection", e => 'test' in e);
        let testEntity = context.createEntity({ test: "testSelections" });
        
        // Exercising
        context.addToSelection(TestSelection, testEntity);

        // Expectation
        expect(TestSelection).toContainEqual(testEntity);
    });

    test("Add to all selections", () => {        
        // Setup
        let context = new poly.Context("default");
        let TestSelection = context.createSelection("TestSelection", e => 'test' in e);
        let testEntity = context.createEntity({ test: "testSelections" });
        
        // Exercising
        context.addToAllSelections(testEntity);

        // Expectation
        expect(TestSelection).toContainEqual(testEntity);

        // Exercising
        let newInSelection = context.createEntity({ test: "newItem" });
        context.addToAllSelections(newInSelection);

        // Expectation
        expect(TestSelection).toContainEqual(testEntity);
        expect(TestSelection).toContainEqual(newInSelection);
    });

    test("Remove from one selection", () => {
        // Setup
        let context = new poly.Context("default");
        let testEntity = context.createEntity({ test: "testSelections" });
        let TestSelection = context.createSelection("TestSelection", e => 'test' in e);
        
        // Exercising
        context.removeFromSelection(TestSelection, testEntity);
    
        // Expectation
        expect(TestSelection).not.toContainEqual(testEntity);
    });

    test("Remove from all selections", () => {
        // Setup
        let context = new poly.Context("default");
        let testEntity = context.createEntity({ test: "testSelections" });
        let TestSelection = context.createSelection("TestSelection", e => 'test' in e);

        // Expectation    
        expect(TestSelection).toContainEqual(testEntity);

        // Exercising
        context.removeFromAllSelections(testEntity);
    
        // Expectation
        expect(TestSelection).not.toContainEqual(testEntity);
    });
});

describe("Test Systems", () => {
    test("Get back a registered system", () => {
        // Setup
        let context = new poly.Context("default");
        poly.applyDefaultSelections(context);

        // Exercising
        let testSystemEntity = context.createEntity(TestSystem, { runOn: poly.EVENTS.ALL, runOrder: 1});
        context.selections["Systems"].push(testSystemEntity);
        
        // Expectations
        expect(context.getSystem("TestSystem").name).toBe("TestSystem");
    });

    test("Add a System to the context", () => {
        // Setup
        let context = new poly.Context("default");
        poly.applyDefaultSelections(context);

        // Exercising
        context.addSystem(TestSystem, poly.EVENTS.ALL, 1);
        
        // Expectations
        expect(context.getSystem("TestSystem").name).toBe("TestSystem");
    });

    test("Get back an unregistered system", () => {
        // Setup
        let context = new poly.Context("default");
        poly.applyDefaultSelections(context);

        // Exercise And
        // Expectations
        expect(() => { context.getSystem("TestSystem") } ).toThrowError(new Error("Unregistered system: TestSystem"));
    });

    test("Delete System loop", () => {
        // Setup 
        let context = new poly.Context("default");
        let entityToDelete = context.createEntity({ name: "test" });
        context.createSelection("Systems", e => 'runOn' in e && 'order' in e, (a, b) => a.order - b.order);
        context.createSelection("Deletables", e => 'toDelete' in e);
        context.addSystem(poly.DeleteSystem, poly.EVENTS.ALL, 101);
        
        // Exercising
        entityToDelete.toDelete = true;
        context.updateAllSelections(entityToDelete);
        context.run(poly.EVENTS.ALL);

        // Expectation
        expect(poly.getContext("default").entities).not.toContainEqual(entityToDelete);
    });

    test("Delete Temporary components System loop", () => {
        // Setup
        let context = new poly.Context("default");
        let entityWithTmpComponent = context.createEntity({ name: "test" });
        poly.applyDefaultSelections(context);
        context.addSystem(poly.DeleteTmpSystem, poly.EVENTS.ALL, 100);
        
        // Exercising
        entityWithTmpComponent.tmpComponent = "testComponent";
        context.run(poly.EVENTS.ALL);

        // Expectation
        expect(entityWithTmpComponent.tmpComponent).toBeUndefined();
    });

    test("Test Pointer Target System with cursor on entity", () => {
        // Setup
        let context = new poly.Context("default");
        let entityToTarget = context.createEntity({ 
            name: "test", 
            targetable: true, 
            depth: -1000, 
            coordinates: new poly.Coordinates(35, 35, 0),
            bounds: new poly.Bounds(20, 20) 
        });
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerTargetSystem, poly.EVENTS.POINTER_INPUT, 1);

        // Exercising
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(40, 40, 0);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(pointer.target.name).toBe(entityToTarget.name);
    });
    
    test("Test Pointer Target System with cursor on void", () => {
        // Setup 
        let context = new poly.Context("default");
        let entityNotToTarget = context.createEntity({ 
            name: "test", 
            targetable: true, 
            depth: -1000, 
            coordinates: new poly.Coordinates(40, 40, 0),
            bounds: new poly.Bounds(20, 20) 
        });
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerTargetSystem, poly.EVENTS.POINTER_INPUT, 1);

        // Exercising
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(80, 80, 0);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(pointer.target).toBeUndefined();
    });

    test("Hoverable entity behaviour", () => {
        // Setup
        let context = new poly.Context("default");
        let entityToHover = context.createEntity({
            name: "test", 
            color: "blue", 
            targetable: true,
            depth: -1000,
            hoverStyle: {color: "red"},
            hoverGroup: "testGroup", 
            bounds: new poly.Bounds(30, 30, 20, 20)
        });
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.HoverSystem, poly.EVENTS.POINTER_INPUT, 41);

        // Exercising
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(40, 40, 0);
        pointer.target = entityToHover;
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(entityToHover.color).toBe("red");

        // Exercising
        pointer.target = null;
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(entityToHover.color).toBe("blue");
    });

    test("Focus on an element, when pressing the item", () => {
        // Setup
        let context = new poly.Context("default");
        let entityToFocus = context.createEntity({
            name: "test", 
            color: "blue", 
            targetable: true,
            depth: -1000,
            richText: {
                text: "hello", 
                editable: true,
                focusStyle: {color: "red"},
            }, 
            bounds: new poly.Bounds(30, 30, 20, 20)
        });
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.FocusSystem, poly.EVENTS.POINTER_INPUT, 41);

        // Exercising
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(40, 40, 0);
        pointer.tmpPressed = 0;
        pointer.target = entityToFocus;
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectations
        expect(entityToFocus.color).toBe("red");
    })

    test("Click on an Element", () => {
        // Setup
        let context = new poly.Context("default");
        let entityToClick = context.createEntity({ 
            name: "test", 
            targetable: true, 
            depth: -1000, 
            bounds: new poly.Bounds(100, 100, 40, 40),
            clickable: true 
        });
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerClickSystem, poly.EVENTS.POINTER_INPUT, 41);
  
        // Exercise mouse down
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(120, 120, 0);
        pointer.tmpPressed = 0;
        pointer.target = entityToClick;
        context.run(poly.EVENTS.POINTER_INPUT);
        expect(pointer.pressTarget).toBe(entityToClick);

        // Exercise mouse up
        pointer.tmpReleased = 0;
        pointer.tmpPressed = undefined;
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(entityToClick.tmpClickBy).toBe(pointer);
        expect(pointer.tmpClick).toBe(entityToClick);
    });

    test("Move by dragging an element", () => {
        // Setup
        let context = new poly.Context("default");
        let entityToDrag = context.createEntity({ 
            name: "test", 
            targetable: true, 
            depth: -1000, 
            bounds: new poly.Bounds(40, 40),
            coordinates: new poly.Coordinates(200, 200, 0),
            draggable: true 
        });
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerDragSystem, poly.EVENTS.POINTER_INPUT, 41);

        // Setup click on element
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(200, 200, 0);
        pointer.buttons = 0;
        pointer.dragging = entityToDrag;

        // Exercise
        pointer.coordinates = new poly.Coordinates(300, 300, 0);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(entityToDrag.coordinates.x).toBe(300);
        expect(entityToDrag.coordinates.y).toBe(300);
    });

    test("Drop a dragged item", () => {
        // Setup
        let context = new poly.Context("default");
        let entityToDrop = context.createEntity({ 
            name: "test", 
            targetable: true, 
            depth: -1000, 
            bounds: new poly.Bounds(40, 40),
            coordinates: new poly.Coordinates(180, 180, 0),
            draggable: true 
        });
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerDragSystem, poly.EVENTS.POINTER_INPUT, 41);

        // Setup dragged element 
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(50, 50, 0);
        pointer.buttons = 0;
        pointer.dragging = entityToDrop;
        entityToDrop.draggedBy = pointer;

        // Exercise
        pointer.tmpReleased = 0;
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(pointer.dragging).toBeUndefined();
        expect(entityToDrop.draggedBy).toBeUndefined();
        expect(entityToDrop.coordinates.x).toBe(50);
        expect(entityToDrop.coordinates.y).toBe(50);
    });

    test("Toggle a button in a togglable group", () => {
        // Setup
        let context = new poly.Context("default");
        let toggleA = context.createEntity({
            toggleGroup: "testToggle",
            toggleStyle: {
                backgroundColor: [200, 200, 200]
            },
            coordinates: new poly.Coordinates(25, 25, 0),
            bounds: new poly.Bounds(10, 10)
        });
        let toggleB = context.createEntity({
            toggleGroup: "testToggle",
            toggleStyle: {
                backgroundColor: [200, 200, 200]
            },
            coordinates: new poly.Coordinates(25, 25, 0),
            bounds: new poly.Bounds(20, 20, 10, 10)
        })
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.ToggleSystem, poly.EVENTS.POINTER_INPUT, 41);

        // Exercise
        let pointer = context.selections["Pointers"][0];
        pointer.tmpClick = toggleB;
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation
        expect(toggleA.toggled).toBeUndefined();
        expect(toggleB.toggled).toBe(1);
    });

    afterEach(() => {
        poly.getContext("default").clearContext();
    });
});

describe("Test defined Components", () => {
    test("Test coordinates absolute movement ", () => {
        // Setup
        let coordinates = new poly.Coordinates(10, 20, 0);

        // Exercise
        coordinates.moveTo(35, 45);

        // Expectation
        expect(coordinates.x).toBe(35);
        expect(coordinates.y).toBe(45);
    });

    test("Test line coordinates absolute movement ", () => {
        // Setup
        let coordinates = new poly.LineCoordinates(10, 20, 30, 40);

        // Exercise
        coordinates.moveTo(35, 45);

        // Expectation
        expect(coordinates.x).toBe(35);
        expect(coordinates.y).toBe(45);
    });

    test("Test coordinates relative movement ", () => {
        // Setup
        let coordinates = new poly.Coordinates(30, 15, 0);

        // Exercise
        coordinates.moveBy(30, 20);

        // Expectation
        expect(coordinates.x).toBe(60);
        expect(coordinates.y).toBe(35);
    });
    
    test("Test line coordinates relative movement ", () => {
        // Setup
        let coordinates = new poly.LineCoordinates(20, 10, 30, 20);

        // Exercise
        coordinates.moveBy(30, 20);

        // Expectation
        expect(coordinates.x).toBe(50);
        expect(coordinates.y).toBe(30);
    });


    test("Test Distance between coordinates", () => {
        // Setup
        let coordinatesStart = new poly.Coordinates(1, 3, 0);
        let coordinatesEnd = new poly.Coordinates(4, 7, 0);

        // Exercising
        let distance = coordinatesStart.distance(coordinatesEnd);

        // Expectation
        expect(distance).toBe(5);
    });

    test("Test Angle between Bounds an Coordinates", () => {
        // Setup
        let coordinates = [
            new poly.Coordinates(5, 7, 0),
            new poly.Coordinates(0, 5, 0),
            new poly.Coordinates(5, -3, 0)
        ];
        let expectations = [45, 90, 315];
        let coord = new poly.Coordinates(0, 2, 0);        

        // Exercising
        let angleInDegrees = [];
        for (let i = 0; i < coordinates.length; i++)
            angleInDegrees.push(coordinates[i].angleBoundsDegrees(coord));

        // Expectation
        for (let i = 0; i < coordinates.length; i++)
            expect(angleInDegrees[i]).toBe(expectations[i]);
    });    

    test("Moved LineCoordinates changes start and end position", () => {
        // Setup
        let lineCoordinate = new poly.LineCoordinates(0, 0, 1, 1);

        // Exercising
        lineCoordinate.moveTo(2.5, 2.5);

        // Expectation
        expect(lineCoordinate.x1).toBe(2.5);
        expect(lineCoordinate.y1).toBe(2.5);
        expect(lineCoordinate.x2).toBe(3.5);
        expect(lineCoordinate.y2).toBe(3.5);
    });

    test("Grow LineCoordinates horizontally", () => {
        // Setup
        let lineCoordinate = new poly.LineCoordinates(0, 0, 1, 1);

        // Exercising 
        lineCoordinate.growW(2);

        // Expectation
        expect(lineCoordinate.x1).toBe(-1);
        expect(lineCoordinate.y1).toBe(0);
        expect(lineCoordinate.x2).toBe(2);
        expect(lineCoordinate.y2).toBe(1);
    });

    test("Grow LineCoordinates vertically", () => {
        // Setup
        let lineCoordinate = new poly.LineCoordinates(0, 0, 1, 1);

        // Exercising 
        lineCoordinate.growH(4);

        // Expectation
        expect(lineCoordinate.x1).toBe(0);
        expect(lineCoordinate.y1).toBe(-2);
        expect(lineCoordinate.x2).toBe(1);
        expect(lineCoordinate.y2).toBe(3);
    });

    test("Bound contains coordinates", () => {
        // Setup
        let context = new poly.Context("default");
        let bound = new poly.Bounds(20, 40, 6, 6);
        let entity = context.createEntity({coordinates: new poly.Coordinates(20, 40, 0), bounds: bound});
        let coordinate = new poly.Coordinates(21, 42, 0);        

        // Exercising
        let isCoordinateContainedInBounds = poly.Bounds.contains(entity, coordinate);

        // Expectation
        expect(isCoordinateContainedInBounds).toBe(true);
    });

    test("Bound does not contains coordinates", () => {
        // Setup
        let context = new poly.Context("default");
        let bound = new poly.Bounds(5, 6);
        let entity = context.createEntity({coordinates: new poly.Coordinates(42, 35, 0), bounds: bound});
        let coordinate = new poly.Coordinates(5, 5, 0);

        // Exercising
        let isCoordinateContainedInBounds = poly.Bounds.contains(entity, coordinate);

        // Expectation
        expect(isCoordinateContainedInBounds).toBe(false);
    });

    afterEach(() => {
        poly.getContext("default").clearContext();
    });
});

describe("Test Factories", () => {    
    test("Make an existing entity a 'Button' using Factory", () => {
        // Setup
        let context = new poly.Context("default");
        let testButton = context.createEntity({ name: "test", shape: poly.SHAPE.ELLIPSE });

        // Pre-expectation
        expect(testButton.depth).toBeUndefined();

        // Exercising
        testButton = new poly.Button(context, new poly.RichText('', null, 10, 10, 4, true), 100, 0, testButton);

        // Expectation
        expect(testButton.depth).toBe(0);
        expect(testButton.shape).toBe(poly.SHAPE.ELLIPSE);
    });

    test("Create Shape using Factory", () => {
        // Setup
        let context = new poly.Context("default");
        let circleShape = context.createEntity({ name: "circle" });

        // Exercising
        circleShape = new poly.Shape(context, poly.SHAPE.ELLIPSE, 10, 20, circleShape);

        // Expectation
        expect(circleShape.name).toBe("circle");
        expect(circleShape.shape).toBe(poly.SHAPE.ELLIPSE);
    });

    test("Compute Bounds on point cloud", () => {
        // Setup
        let targetBound = new poly.Bounds(5, 10);
        let pointArray = [[-2.5, 5], [0, 5], [2.5, 5], [0, 0], [0, -5]];

        // Exercising
        let calculatedBound = poly.computeBound2D(pointArray);

        // Expectation
        expect(calculatedBound.width).toBe(targetBound.width);
        expect(calculatedBound.height).toBe(targetBound.height);
    });

    test("Compute Coordinate center on point cloud", () => {
        // Setup
        let targetCoordinate = new poly.Coordinates(3.5, 2.5, 0);
        let pointArray = [[1, 5], [3.5, 5], [6, 5], [3.5, 0]];

        // Exercising
        let calculatedCoord = poly.computeCenter2D(pointArray);

        // Expectation
        expect(calculatedCoord.x).toBe(targetCoordinate.x);
        expect(calculatedCoord.y).toBe(targetCoordinate.y);
    });

    test("Create a Complex Shape using Factory", () => {
        // Setup
        let context = new poly.Context("default");
        let customShape = context.createEntity({ name: "polyline" });
        let customBounds = new poly.Bounds(4, 2);
        let customCoord = new poly.Coordinates(-1, 0);

        // Exercising
        customShape = new poly.CustomShape(context, [[-3, 1], [1, 0], [-3, -1]], customShape);

        // Expectation
        expect(customShape.name).toBe("polyline");
        expect(customShape.shape).toBe(poly.SHAPE.POLYLINE);
        expect(customShape.bounds.width).toBe(customBounds.width);
        expect(customShape.bounds.height).toBe(customBounds.height);
        expect(customShape.coordinates.x).toBe(customCoord.x);
        expect(customShape.coordinates.y).toBe(customCoord.y);
    });

    test("Create a Radio Button", () => {
        // Setup
        let context = new poly.Context("default");
        let radioToggle = context.createEntity({ name: "radio" });

        // Exercising
        radioToggle = new poly.RadioButton(context, "testRadio", 20, 30, "radio", {backgroundColor: [12, 34, 56]}, radioToggle);

        // Expectation
        expect(radioToggle.name).toBe("radio");
        expect(radioToggle.shape).toBe(poly.SHAPE.RECTANGLE);
        expect(radioToggle.toggleStyle.backgroundColor).toStrictEqual([12, 34, 56]);
    });

    test("Create a Checkbox", () => {
       // Setup
       let context = new poly.Context("default");
       let checkboxToggle = context.createEntity({ name: "checkbox" });

       // Exercising
       checkboxToggle = new poly.CheckBox(context, "testRadio", 50, 40, "check", {backgroundColor: [0, 0, 0]}, checkboxToggle);

       // Expectation
       expect(checkboxToggle.name).toBe("checkbox");
       expect(checkboxToggle.shape).toBe(poly.SHAPE.RECTANGLE);
       expect(checkboxToggle.toggleStyle.backgroundColor).toStrictEqual([0, 0, 0]);
    });

    afterEach(() => {
        if(poly.getContext("default"))
            poly.getContext("default").clearContext();
    });
});

describe("Test interaction loop", () => {
    test("ManageEvent mouseup modifies context pointer", () => {
        // Setup
        let context = new poly.Context("default");
        let mouseupEvent = new MouseEvent("mouseup", {view: window, button: 0});
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);

        // Exercise
        context.manageEvent("mouseup", mouseupEvent);

        // Expectation
        let pointer = context.selections["Pointers"][0];
        expect(pointer.tmpReleased).toBe(0);
    });

    test("MetaSystem triggers all context systems", () => {
        // Setup 
        let defaultcontext = new poly.Context("default");
        let altContext = new poly.Context("alternative");

        let testEntity = defaultcontext.createEntity( { name: "test", modified: false } );
        let testAlternativeEntity = altContext.createEntity({ name: "alttest", modified: false });

        poly.addViewPointerKeyboardToContext(defaultcontext);
        poly.addViewPointerKeyboardToContext(altContext);
        poly.applyDefaultSelections(defaultcontext);
        poly.applyDefaultSelections(altContext);
        
        defaultcontext.addSystem(TestSystem, poly.EVENTS.POINTER_INPUT, 1);
        altContext.addSystem(TestSystem, poly.EVENTS.KEYBOARD_INPUT, 1);

        // Exercise
        poly.MetaSystem(poly.EVENTS.ALL);

        // Expectation
        expect(testEntity.modified).toBe(true);
        expect(testAlternativeEntity.modified).toBe(true);
    });

    test("MetaSystem triggers all context systems when renderer is started", () => {
        // Setup 
        let defaultContext = new poly.Context("default");

        let testEntity = defaultContext.createEntity( { name: "test", modified: false } );
        let testAlternativeEntity = defaultContext.createEntity({ name: "alt", modified: false });

        poly.addViewPointerKeyboardToContext(defaultContext);
        poly.applyDefaultSelections(defaultContext);
        
        defaultContext.addSystem(TestSystem, poly.EVENTS.POINTER_INPUT, 1);
        defaultContext.addSystem(TestSystemAlt, poly.EVENTS.DISPLAY_OUTPUT, 2);

        defaultContext.renderer_started = true;

        // Exercise
        poly.MetaSystem(poly.EVENTS.ALL);

        // Expectation
        expect(testEntity.modified).toBe(true);
        expect(testAlternativeEntity.modified).toBe(true);
    });

    test("Main loop triggers systems", () => {
        // Setup
        let context = new poly.Context("default");
        let testElement = context.createEntity({ name: "test", modified: false});

        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(TestSystem, poly.EVENTS.POINTER_INPUT, 1);

        // Create a simple mouseUp
        var event = new MouseEvent("mouseup", {
            'view': window,
            'button': 0,
        });
        poly.eventStack.push( { type: "mouseup", event: event } );

        // Exercise
        poly.mainLoop();

        // Expectation
        expect(testElement.modified).toBe(true);
    });

    afterEach(() => {
        poly.getContext("default").clearContext();
    });
});

describe("Interaction tests", () => {
    test("Click on a button", () => {
        // Setup 
        let context = new poly.Context("default");
        let button = new poly.Button(
            context, 
            new poly.RichText('', null, 10, 10, 4, true), 
            200, 
            200, 
            { 
                name: "button0", 
                draggable: true,
                coordinates: new poly.Coordinates(),
                bounds: new poly.Bounds(50, 50)
            });

        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerTargetSystem, poly.EVENTS.ALL, 1);
        context.addSystem(poly.PointerClickSystem, poly.EVENTS.ALL, 2);

        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(220, 220, 0);        

        // Exercise
        pressLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);
        pointer.tmpPressed = null;
        releaseLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);
        
        // Expectations
        expect(pointer.target).toBe(button);
        expect(pointer.tmpClick).toBe(button);
        expect(button.tmpClickBy).toBe(pointer);
    });

    test("Click & drag a Shape", () => {
        // Setup
        let context = new poly.Context("default");
        let shape = new poly.Shape(context, poly.SHAPE.RECTANGLE, 50, 50, 
            { 
                name: "rect0",
            });
        shape.coordinates.moveTo(75, 75);
        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerTargetSystem, poly.EVENTS.POINTER_INPUT, 1);
        context.addSystem(poly.PointerClickSystem, poly.EVENTS.POINTER_INPUT, 30);
        context.addSystem(poly.PointerDragSystem, poly.EVENTS.POINTER_INPUT, 40);
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(100, 100);

        // Exercise press on shape
        pressLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation press on shape
        expect(pointer.pressTarget).toBe(shape);

        // Exercise start dragging
        pointer.tmpPressed = null;
        pointer.coordinates = new poly.Coordinates(90, 90);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation start dragging
        expect(pointer.dragging).toBe(shape);
        expect(shape.coordinates.x).toBe(90);
        expect(shape.coordinates.y).toBe(90);

        // Exercise drop shape
        releaseLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectation drop shape
        expect(pointer.dragging).not.toBe(shape);
        expect(shape.coordinates.x).toBe(90);
        expect(shape.coordinates.y).toBe(90);
    });

    test("test asynchrone #u-", async () => {
        let context = new poly.Context("default");
        let shape = new poly.Shape(context, poly.SHAPE.RECTANGLE, 50, 50, { name: "rect1" })
        shape.coordinates.moveTo(125, 125)
        shape.depth = -1000

        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(75, 75);
        context.run(poly.EVENTS.POINTER_INPUT);
        await new Promise(resolve => setTimeout(resolve, 1000));
        let shape2 = new poly.Shape(context, poly.SHAPE.RECTANGLE, 50, 50, { name: "rect2" });
        shape2.coordinates.moveTo(225, 125);
        shape2.depth = -1000;
        pointer.coordinates = new poly.Coordinates(75, 75);
        context.run(poly.EVENTS.POINTER_INPUT);
    });

    test("Test checkbox button group", () => {
        // Setup 
        let context = new poly.Context("default");
        let check1 = testCheckBox(context, "check1", "test");
        let check2 = testCheckBox(context, "check2", "test");
        let check3 = testCheckBox(context, "check3", "test");
        let bound = new poly.Bounds(20, 10);

        check1.coordinates.moveTo(100, 100, 0);
        check2.coordinates.moveTo(100, 200, 0);
        check3.coordinates.moveTo(100, 300, 0);

        check1.bounds = bound;
        check2.bounds = bound;
        check3.bounds = bound;

        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerTargetSystem, poly.EVENTS.ALL, 1);
        context.addSystem(poly.PointerClickSystem, poly.EVENTS.ALL, 2);
        context.addSystem(poly.ToggleSystem, poly.EVENTS.ALL, 3);
        context.addSystem(poly.DeleteTmpSystem, poly.EVENTS.POINTER_INPUT, 4);
  
        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(100, 200, 0);

        // Exercise
        pressLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);
        releaseLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectations
        expect(pointer.target).toBe(check2);
        expect(check1.toggled).toBeUndefined();
        expect(check2.toggled).toBe(1);
        expect(check3.toggled).toBeUndefined();

        // Setup
        pointer.coordinates.moveTo(100, 100, 0);

        // Exercise
        pressLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);
        releaseLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectations
        expect(pointer.target).toBe(check1);
        expect(check1.toggled).toBe(1);
        expect(check2.toggled).toBe(1);
        expect(check3.toggled).toBeUndefined();

    });

    test("Test radio button group", () => {
        // Setup 
        let context = new poly.Context("default");
        let radio1 = testRadio(context, "radio1", "test");
        let radio2 = testRadio(context, "radio2", "test");
        let radio3 = testRadio(context, "radio3", "test");
        let bound = new poly.Bounds(20, 10);

        radio1.coordinates.moveTo(100, 100, 0);
        radio2.coordinates.moveTo(100, 200, 0);
        radio3.coordinates.moveTo(100, 300, 0);
        
        radio1.bounds = bound;
        radio2.bounds = bound;
        radio3.bounds = bound;

        poly.addViewPointerKeyboardToContext(context);
        poly.applyDefaultSelections(context);
        context.addSystem(poly.PointerTargetSystem, poly.EVENTS.ALL, 1);
        context.addSystem(poly.PointerClickSystem, poly.EVENTS.ALL, 2);
        context.addSystem(poly.ToggleSystem, poly.EVENTS.ALL, 3);

        let pointer = context.selections["Pointers"][0];
        pointer.coordinates = new poly.Coordinates(100, 200, 0);

        // Exercise
        pressLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);
        pointer.tmpPressed = null;
        releaseLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);

        // Expectations
        expect(pointer.target).toBe(radio2);
        expect(radio1.toggled).toBeUndefined();
        expect(radio2.toggled).toBe(1);
        expect(radio3.toggled).toBeUndefined();

        pointer.coordinates.moveTo(100, 100, 0);

        // Exercise
        pressLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);
        pointer.tmpPressed = null;
        releaseLeftClick(pointer);
        context.run(poly.EVENTS.POINTER_INPUT);
        expect(true).toBe(true);

        // Expectations
        expect(pointer.target).toBe(radio1);
        expect(radio1.toggled).toBe(1);
        expect(radio2.toggled).toBeUndefined();
        expect(radio3.toggled).toBeUndefined();
    });

    afterEach(() => {
        poly.getContext("default").clearContext();
    });
});

/* Needed extra tests
 * 
 *   Main loop (use calls to see if system were called right on event type ?)
 *   Manage Event
 *   Shortcut and text editing system to test
 *   Renderer call?
 *   
 */