import { Context } from "../src/js/Polyphony.js";
import * as renderSVG from "../src/js/renderSVG.js"

test("Transform color array to rgb string", () => {
    // Setup
    let expectation = "rgb(100,20,30)";
    let value = [100, 20, 30];
    // Exercise
    let calculation = renderSVG.colorArrayToString(value);
    // Expectation
    expect(calculation).toBe(expectation);
});

test("Transform color and transparency array to rgba string", () => {
    // Setup
    let expectation = "rgba(100,20,30,0.5)";
    let value = [100, 20, 30, 0.5];
    // Exercise
    let calculation = renderSVG.colorArrayToString(value);
    // Expectation
    expect(calculation).toBe(expectation);
});

test("Get null from wrong size array", () => {
    // Setup;
    let value = [8, 0, 9, 19, 22];
    // Exercise
    let calculation = renderSVG.colorArrayToString(value);
    // Expectation
    expect(calculation).toBeNull();
});

test("Get null from empty array", () => {
    // Setup
    let value;
    // Exercise
    let calculation = renderSVG.colorArrayToString(value);
    // Expectation
    expect(calculation).toBeNull();
});

test("Propagate change on 1 depth", () => {
    // Setup
    let context = new Context("test");
    let entityA = context.createEntity();
    let entityB = context.createEntity();
    let entityC = context.createEntity();
    let entityD = context.createEntity({ has_changed: true, dependency: [ entityA, entityB ] });

    // Exercise
    renderSVG.DependencyChangePropagation(context);

    // Expectation
    expect(entityA.has_changed).toBe(true);
    expect(entityB.has_changed).toBe(true);
    expect(entityC.has_changed).toBeUndefined();
    expect(entityD.has_changed).toBe(true);
});

test("Propagate change on 2 depth", () => {
    // Setup
    let context = new Context("test");
    let entityA = context.createEntity();
    let entityB = context.createEntity();
    let entityC = context.createEntity();
    let entityD = context.createEntity({ dependency: [ entityC ] });
    let entityE = context.createEntity({ has_changed: true, dependency: [ entityA, entityD ] });

    // Exercise
    renderSVG.DependencyChangePropagation(context);

    // Expectation
    expect(entityA.has_changed).toBe(true);
    expect(entityB.has_changed).toBeUndefined();
    expect(entityC.has_changed).toBe(true);
    expect(entityD.has_changed).toBe(true);
    expect(entityE.has_changed).toBe(true);
});

test("Propagate change on circular dependency", () => {
    // Setup
    let context = new Context("test");
    let entityA = context.createEntity();
    let entityB = context.createEntity();
    let entityC = context.createEntity();
    let entityD = context.createEntity({ dependency: [ entityC ] });
    let entityE = context.createEntity({ has_changed: true, dependency: [ entityA, entityD ] });
    entityC.dependency = [ entityE ];

    // Exercise
    renderSVG.DependencyChangePropagation(context);

    // Expectation
    expect(entityA.has_changed).toBe(true);
    expect(entityB.has_changed).toBeUndefined();
    expect(entityC.has_changed).toBe(true);
    expect(entityD.has_changed).toBe(true);
    expect(entityE.has_changed).toBe(true);
});