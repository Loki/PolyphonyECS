import * as UML from '../src/js/UMLApp.js'
import * as poly from '../src/js/Polyphony.js'

describe("UML helpers test", () => {
    test("Find focusable in one gen children", () => {
        // Setup
        let context = new poly.Context("default");

        let entityParent = context.createEntity({
            bounds: new poly.Bounds(20, 20),
            coordinates: new poly.Coordinates(40, 40)
        });
        let entityChild1 = context.createEntity({ 
            richText: new poly.RichText("", null, 1, 0, 0, 0, false, {test: true}),
            bounds: new poly.Bounds(20, 10),
            coordinates: new poly.Coordinates(40, 35, 0)
        });
        let entityChild2 = context.createEntity({ 
            richText: new poly.RichText("", null, 1, 0, 0, 0, true, {test: true}),
            bounds: new poly.Bounds(20, 10),
            coordinates: new poly.Coordinates(40, 45, 0)
        });

        entityParent.children = [entityChild1, entityChild2];

        // Exercise
        let focusableEntity = UML.findFocusableInChildren(entityParent, new poly.Coordinates(50, 50, 0));

        // Expectations
        expect(focusableEntity).toBe(entityChild2);
    });

    test("Find no focusable in one gen children", () => {
        // Setup
        let context = new poly.Context("default");

        let entityParent = context.createEntity({
            bounds: new poly.Bounds(20, 20),
            coordinates: new poly.Coordinates(40, 40)
        });
        let entityChild1 = context.createEntity({ 
            richText: new poly.RichText("", null, 1, 0, 0, 0, false, {test: true}),
            bounds: new poly.Bounds(20, 10),
            coordinates: new poly.Coordinates(40, 35, 0)
        });
        let entityChild2 = context.createEntity({ 
            bounds: new poly.Bounds(20, 10),
            coordinates: new poly.Coordinates(40, 45, 0)
        });

        entityParent.children = [entityChild1, entityChild2];

        // Exercise
        let focusableEntity = UML.findFocusableInChildren(entityParent, new poly.Coordinates(40, 45, 0));

        // Expectations
        expect(focusableEntity).toBe(null);
    });
});