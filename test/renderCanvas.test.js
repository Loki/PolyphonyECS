import * as renderCanvas from "../src/js/renderCanvas.js"

test("Transform color array to rgb string", () => {
    // setup
    let expectation = "rgb(100,20,30)";
    let value = [100, 20, 30];
    // Exercise
    let calculation = renderCanvas.colorArrayToString(value);
    // Expectation
    expect(calculation).toBe(expectation);
});

test("Transform color and transparency array to rgba string", () => {
    // setup
    let expectation = "rgba(100,20,30,0.5)";
    let value = [100, 20, 30, 0.5];
    // Exercise
    let calculation = renderCanvas.colorArrayToString(value);
    // Expectation
    expect(calculation).toBe(expectation);
});

test("Get null from wrong size array", () => {
    // setup;
    let value = [8, 0, 9, 19, 22];
    // Exercise
    let calculation = renderCanvas.colorArrayToString(value);
    // Expectation
    expect(calculation).toBeNull();
});

test("Get null from empty array", () => {
    // setup
    let value;
    // Exercise
    let calculation = renderCanvas.colorArrayToString(value);
    // Expectation
    expect(calculation).toBeNull();
});